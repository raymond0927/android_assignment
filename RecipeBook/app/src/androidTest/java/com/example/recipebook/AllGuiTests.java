package com.example.recipebook;


import android.support.test.runner.AndroidJUnit4;

import com.example.recipebook.Activity.MainActivity;
import com.example.recipebook.Activity.RegisterActivity;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({SettingUITest.class, RegisterUITest.class , LoginUITest.class, MainUITest.class,RecipeInfoUITest.class})
public class AllGuiTests  {

}