package com.example.recipebook;

import android.content.Intent;
import android.support.test.espresso.Espresso;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.example.recipebook.Activity.LoginActivity;
import com.example.recipebook.Activity.MainActivity;
import com.example.recipebook.Activity.RegisterActivity;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.RootMatchers.withDecorView;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.CoreMatchers.anything;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.Matchers.hasToString;
import static org.hamcrest.core.StringStartsWith.startsWith;

@RunWith(AndroidJUnit4.class)
public class MainUITest {


    @Rule
    public ActivityTestRule<LoginActivity> loginActivityActivityTestRule = new ActivityTestRule<>(LoginActivity.class);

    @Before
    public void  setUp() {
        onView(ViewMatchers.withId(R.id.edt_UserName)).perform(typeText("raymond"), closeSoftKeyboard());
        onView(withId(R.id.edt_Password)).perform(typeText("12345678"), closeSoftKeyboard());
        onView(withId(R.id.btn_Login)).perform(click());
        onView(withText(R.string.login)).check(matches(isDisplayed()));
        try{
            Thread.sleep(3000);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Test
    public void checklogined() {
        onView(withId(R.id.iv_add)).check(matches(isDisplayed()));
        onView(withId(R.id.iv_home)).check(matches(isDisplayed()));
    }


    @Test
    public void modfiyUser() {
        onView(withId(R.id.iv_setting)).perform(click());
        onView(ViewMatchers.withId(R.id.edt_password_modify)).perform(typeText("12345678"), closeSoftKeyboard());
        onView(withId(R.id.edt_new_email_modify)).perform(typeText("test@email.com"), closeSoftKeyboard());
        onView(withId(R.id.btn_modify_user_confirm)).perform(click());
    }

    @Test
    public void modfiyUser_emptyCheck() {
        onView(withId(R.id.iv_setting)).perform(click());
        onView(withId(R.id.btn_modify_user_confirm)).perform(click());
        onView(withText(R.string.password_empty)).inRoot(withDecorView(not(is(loginActivityActivityTestRule.getActivity().getWindow().getDecorView())))).check(matches(isDisplayed()));
        onView(ViewMatchers.withId(R.id.edt_password_modify)).perform(typeText("12345678"), closeSoftKeyboard());
        onView(withId(R.id.btn_modify_user_confirm)).perform(click());
        onView(withText(R.string.new_email_or_newpassword_empty)).inRoot(withDecorView(not(is(loginActivityActivityTestRule.getActivity().getWindow().getDecorView())))).check(matches(isDisplayed()));
    }

    @Test
    public void checkbutton() {
        onView(withId(R.id.iv_favourite)).perform(click());
        try{
            Thread.sleep(1000);
        }catch (Exception e){
            e.printStackTrace();
        }
        onView(withId(R.id.iv_member)).perform(click());
        try{
            Thread.sleep(1000);
        }catch (Exception e){
            e.printStackTrace();
        }
        onView(withId(R.id.iv_home)).perform(click());
        try{
            Thread.sleep(1000);
        }catch (Exception e){
            e.printStackTrace();
        }
        onView(withId(R.id.iv_add)).perform(click());
        onView(withId(R.id.swh_Chinese_recipe)).check(matches(isDisplayed()));
        onView(withId(R.id.btn_edit_back)).perform(click());
    }

    @Test
    public void gotoRecipeInfo() {
        onData(anything())
                .inAdapterView(withId(R.id.lv_recipe))
                .atPosition(0)
                .perform(click());
        onView(withId(R.id.iv_comment)).check(matches(isDisplayed()));
    }

    @Test
    public void checkLogout() {
        onView(withId(R.id.iv_logout)).perform(click());
        onView(withText("OK")).perform(click());
        onView(withId(R.id.btn_Login)).check(matches(isDisplayed()));
    }

}

