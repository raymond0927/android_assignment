package com.example.recipebook;

import android.support.test.espresso.Espresso;
import android.support.test.espresso.action.ViewActions;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.example.recipebook.Activity.LoginActivity;
import com.example.recipebook.Activity.SettingActivity;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.pressBack;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.isRoot;
import static android.support.test.espresso.matcher.ViewMatchers.withContentDescription;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.CoreMatchers.anything;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.AllOf.allOf;

@RunWith(AndroidJUnit4.class)
public class SettingUITest {

    @Rule
    public ActivityTestRule<SettingActivity> settingActivityActivityTestRule = new ActivityTestRule<>(SettingActivity.class);

    @Before
    public void setup() {
        settingActivityActivityTestRule.getActivity().setLocale("en");
    }

    @Test
    public void changelanuage() {
        onView(withId(R.id.iv_login_setting)).perform(click());
        onView(withId(R.id.spinner_lan)).perform(click());
        onData(allOf(is(instanceOf(String.class)), is("zh")))
                .perform(click());
        onView(withId(R.id.tv_login_title)).check(matches(withText("登入")));
    }



    @After
    public void revertlanuage() {
        onView(withId(R.id.iv_login_setting)).perform(click());
        onView(withId(R.id.spinner_lan)).perform(click());
        onData(allOf(is(instanceOf(String.class)), is("en")))
                .perform(click());
    }

}
