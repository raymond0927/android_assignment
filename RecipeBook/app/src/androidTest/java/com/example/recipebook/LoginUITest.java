package com.example.recipebook;

import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.example.recipebook.Activity.LoginActivity;
import com.example.recipebook.Activity.MainActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;


import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.RootMatchers.withDecorView;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;

@RunWith(AndroidJUnit4.class)
public class LoginUITest {
    @Rule
    public ActivityTestRule<LoginActivity> loginActivityRule = new ActivityTestRule<>(LoginActivity.class);

    @Test
    public void loginWithCorrectInfo() {
        onView(ViewMatchers.withId(R.id.edt_UserName)).perform(typeText("raymond"), closeSoftKeyboard());
        onView(withId(R.id.edt_Password)).perform(typeText("12345678"), closeSoftKeyboard());
        onView(withId(R.id.btn_Login)).perform(click());
        onView(withText(R.string.login)).check(matches(isDisplayed()));
        try{
            Thread.sleep(3000);
        }catch (Exception e){
            e.printStackTrace();
        }
        onView(withId(R.id.iv_add)).check(matches(isDisplayed()));
    }

    @Test
    public void emptyToastCheck() {
        onView(withId(R.id.btn_Login)).perform(click());
        onView(withText(R.string.username_empty)).inRoot(withDecorView(not(is(loginActivityRule.getActivity().getWindow().getDecorView())))).check(matches(isDisplayed()));
        onView(ViewMatchers.withId(R.id.edt_UserName)).perform(typeText("raymond"), closeSoftKeyboard());
        onView(withId(R.id.btn_Login)).perform(click());
        onView(withText(R.string.password_empty)).inRoot(withDecorView(not(is(loginActivityRule.getActivity().getWindow().getDecorView())))).check(matches(isDisplayed()));
    }
}

