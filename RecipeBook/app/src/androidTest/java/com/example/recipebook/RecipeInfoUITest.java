package com.example.recipebook;

import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.example.recipebook.Activity.LoginActivity;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.clearText;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.RootMatchers.withDecorView;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.CoreMatchers.anything;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;

@RunWith(AndroidJUnit4.class)
public class RecipeInfoUITest {

    @Rule
    public ActivityTestRule<LoginActivity> loginActivityActivityTestRule = new ActivityTestRule<>(LoginActivity.class);

    @Before
    public void  setUp() {
        onView(ViewMatchers.withId(R.id.edt_UserName)).perform(typeText("raymond"), closeSoftKeyboard());
        onView(withId(R.id.edt_Password)).perform(typeText("12345678"), closeSoftKeyboard());
        onView(withId(R.id.btn_Login)).perform(click());
        onView(withText(loginActivityActivityTestRule.getActivity().getResources().getString(R.string.login))).check(matches(isDisplayed()));
        try{
            Thread.sleep(3000);
        }catch (Exception e){
            e.printStackTrace();
        }
        onData(anything())
                .inAdapterView(withId(R.id.lv_recipe))
                .atPosition(0)
                .perform(click());
        try{
            Thread.sleep(2000);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Test
    public void gotoRecipeInfo() {
        onView(withId(R.id.iv_comment)).check(matches(isDisplayed()));
    }

    @Test
    public void modifyRecipe() {
        onView(withId(R.id.btn_info_add)).perform(click());
        onView(withId(R.id.swh_Chinese_recipe)).check(matches(isDisplayed()));
        onView(withId(R.id.edt_recipe_name)).perform(clearText());
        onView(withId(R.id.edt_recipe_name)).perform(typeText("hihi12345"), closeSoftKeyboard());
        onView(withId(R.id.btn_edit_confirm)).perform(scrollTo()).perform(click());
        try{
            Thread.sleep(2000);
        }catch (Exception e){
            e.printStackTrace();
        }
        onView(withId(R.id.tv_recipe_info_name)).check(matches(withText("hihi12345")));

    }

    @Test
    public void modifyRecipe_emptyCheck() {
        onView(withId(R.id.btn_info_add)).perform(click());
        onView(withId(R.id.swh_Chinese_recipe)).perform(click());
        onView(withId(R.id.edt_recipe_methods)).perform(clearText());
        onView(withId(R.id.btn_edit_confirm)).perform(scrollTo()).perform(click());
        onView(withText(R.string.methods_empty)).inRoot(withDecorView(not(is(loginActivityActivityTestRule.getActivity().getWindow().getDecorView())))).check(matches(isDisplayed()));
        onView(withId(R.id.edt_recipe_ingredients)).perform(clearText());
        onView(withId(R.id.btn_edit_confirm)).perform(scrollTo()).perform(click());
        onView(withText(R.string.ingredients_empty)).inRoot(withDecorView(not(is(loginActivityActivityTestRule.getActivity().getWindow().getDecorView())))).check(matches(isDisplayed()));
        onView(withId(R.id.edt_recipe_name)).perform(clearText());
        onView(withId(R.id.btn_edit_confirm)).perform(scrollTo()).perform(click());
        onView(withText(R.string.title_empty)).inRoot(withDecorView(not(is(loginActivityActivityTestRule.getActivity().getWindow().getDecorView())))).check(matches(isDisplayed()));
    }

    @Test
    public void revertRecipe() {
        onView(withId(R.id.btn_info_add)).perform(click());
        onView(withId(R.id.swh_Chinese_recipe)).check(matches(isDisplayed()));
        onView(withId(R.id.edt_recipe_name)).perform(clearText());
        onView(withId(R.id.edt_recipe_name)).perform(typeText("Spaghetti Carbonara"), closeSoftKeyboard());
        onView(withId(R.id.btn_edit_confirm)).perform(scrollTo()).perform(click());
        try{
            Thread.sleep(2000);
        }catch (Exception e){
            e.printStackTrace();
        }
        onView(withId(R.id.tv_recipe_info_name)).check(matches(withText("Spaghetti Carbonara")));
    }

    @Test
    public void addComment_emptyCheck() {
        onView(withId(R.id.iv_comment)).perform(click());
        try{
            Thread.sleep(1000);
        }catch (Exception e){
            e.printStackTrace();
        }
        onView(withId(R.id.btn_info_add)).perform(click());
        onView(withId(R.id.btn_comment_confirm)).perform(click());
        onView(withText(R.string.comment_empty)).inRoot(withDecorView(not(is(loginActivityActivityTestRule.getActivity().getWindow().getDecorView())))).check(matches(isDisplayed()));

    }

    @Test
    public void checkbutton() {
        onView(withId(R.id.iv_comment)).perform(click());
        try{
            Thread.sleep(1000);
        }catch (Exception e){
            e.printStackTrace();
        }
        onView(withId(R.id.lv_comment)).check(matches(isDisplayed()));
        onView(withId(R.id.iv_recipe)).perform(click());
        try{
            Thread.sleep(1000);
        }catch (Exception e){
            e.printStackTrace();
        }
        onView(withId(R.id.tv_recipe_info_name)).check(matches(isDisplayed()));
    }

    @Test
    public void goBack(){
        onView(withId(R.id.btn_info_back)).perform(click());
        onView(withId(R.id.iv_home)).check(matches(isDisplayed()));
    }


}
