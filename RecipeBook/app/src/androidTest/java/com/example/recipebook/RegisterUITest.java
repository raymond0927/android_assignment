package com.example.recipebook;

import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.example.recipebook.Activity.LoginActivity;
import com.example.recipebook.Activity.RegisterActivity;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.RootMatchers.withDecorView;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withContentDescription;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;

@RunWith(AndroidJUnit4.class)
public class RegisterUITest {

    @Rule
    public ActivityTestRule<LoginActivity> loginActivityRule = new ActivityTestRule<>(LoginActivity.class);

    @Before
    public void moveToRegisterView() {
        onView(withId(R.id.btn_register)).perform(click());
    }


    @Rule
    public ActivityTestRule<RegisterActivity> registerActivityRule = new ActivityTestRule<>(RegisterActivity.class);

    @Test
    public void goBack(){
        onView(withId(R.id.iv_register_back)).perform(click());
        onView(withId(R.id.btn_Login)).check(matches(isDisplayed()));
    }

    @Test
    public void createAccount() {
        onView(withId(R.id.edt_register_UserName)).perform(typeText("yoyo"), closeSoftKeyboard());
        onView(withId(R.id.edt_register_email)).perform(typeText("email.com"), closeSoftKeyboard());
        onView(withId(R.id.edt_register_Password)).perform(typeText("123"), closeSoftKeyboard());
        onView(withId(R.id.edt_register_ConfirmPassword)).perform(typeText("123"), closeSoftKeyboard());
        onView(withId(R.id.edt_register_email)).check(matches(withText("email.com")));
        onView(withId(R.id.btn_register_confirm)).perform(click());
        try{
            Thread.sleep(2000);
        }catch (Exception e){
            e.printStackTrace();
        }
        onView(withId(R.id.btn_Login)).check(matches(isDisplayed()));
    }

    @Test
    public void emptyToastCheck() {
        onView(withId(R.id.btn_register_confirm)).perform(click());
        onView(withText(R.string.username_empty)).inRoot(withDecorView(not(is(registerActivityRule.getActivity().getWindow().getDecorView())))).check(matches(isDisplayed()));
        onView(withId(R.id.edt_register_UserName)).perform(typeText("yoyo"), closeSoftKeyboard());
        onView(withId(R.id.btn_register_confirm)).perform(click());
        onView(withText(R.string.email_empty)).inRoot(withDecorView(not(is(registerActivityRule.getActivity().getWindow().getDecorView())))).check(matches(isDisplayed()));
        onView(withId(R.id.edt_register_email)).perform(typeText("email.com"), closeSoftKeyboard());
        onView(withId(R.id.btn_register_confirm)).perform(click());
        onView(withText(R.string.password_empty)).inRoot(withDecorView(not(is(registerActivityRule.getActivity().getWindow().getDecorView())))).check(matches(isDisplayed()));
        onView(withId(R.id.edt_register_Password)).perform(typeText("123"), closeSoftKeyboard());
        onView(withId(R.id.edt_register_ConfirmPassword)).perform(typeText("1234"), closeSoftKeyboard());
        onView(withId(R.id.btn_register_confirm)).perform(click());
        onView(withText(R.string.password_not_match)).inRoot(withDecorView(not(is(registerActivityRule.getActivity().getWindow().getDecorView())))).check(matches(isDisplayed()));
    }
}
