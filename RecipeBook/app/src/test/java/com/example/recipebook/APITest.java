package com.example.recipebook;

import android.app.Activity;
import android.content.Context;

import com.example.recipebook.API.APIInterface;
import com.example.recipebook.API.API_Manager;
import com.example.recipebook.API.JSON_CLASS;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class APITest implements APIInterface {

    private API_Manager api_manager;
    private GlobalVar globalVar;
    private String domain = "immense-castle-74565.herokuapp.com";
    private String token;


    @Mock
    Activity activity;

    @Before
    public void initTests() {
        globalVar= (GlobalVar) activity.getApplicationContext();
        api_manager = new API_Manager(this, activity, globalVar);
    }

    @Test
    public void testLogin() {
        JSON_CLASS.PostLogin postItems = new JSON_CLASS.PostLogin();
        postItems.password ="12345678";
        postItems.username = "raymond";

        final String inputJson = new Gson().toJson(postItems);
        final String url = "http://" + domain + "/api/users/login";
        String jsonResult = "";
        try{
            jsonResult = api_manager.postHttp(url,inputJson);
        }catch (Exception e){
            e.printStackTrace();
        }

        assertTrue(loginResult(jsonResult));
    }

    @Test
    public void testgetMethod() {
        String url = "http://" + domain + "/api/favourite/getFavouriteListByUserID?token=" + getToken();
        String jsonResult = "";
        try{
            jsonResult = api_manager.getHttp(url);
        }catch (Exception e){
            e.printStackTrace();
        }

        assertTrue(jsonResult.length()>0);
    }

    @Test
    public void testpostMethod() {
        JSON_CLASS.PostComment postItems = new JSON_CLASS.PostComment();
        postItems.recipeId = 22;
        postItems.comment_chi = "test123";
        postItems.comment_eng = "testabc";

        final String inputJson = new Gson().toJson(postItems);
        final String url = "http://" + domain + "/api/comment/createComment?token=" + getToken();
        String jsonResult = "";
        try{
            jsonResult = api_manager.postHttp(url,inputJson);
        }catch (Exception e){
            e.printStackTrace();
        }

        assertTrue(jsonResult.indexOf("Success")>-1);
    }

    @Test
    public void testputMethod() {
        JSON_CLASS.PutUser putItems = new JSON_CLASS.PutUser();
        putItems.username = "raymond";
        putItems.password = "12345678";
        putItems.newEmail = "hihi@email.com";


        final String inputJson = new Gson().toJson(putItems);

        String url = "http://" +domain + "/api/users/modifyPassword" ;
        String jsonResult = "";
        try{
            jsonResult = api_manager.putHttp(url,inputJson);
        }catch (Exception e){
            e.printStackTrace();
        }

        assertTrue(jsonResult.indexOf("Success")>-1);
    }

    @Test
    public void testdelMethod() {
        JSON_CLASS.DelComment delComment = new JSON_CLASS.DelComment();
        delComment.commentid = 812;

        final String inputJson = new Gson().toJson(delComment);

        String url = "http://" +domain + "/api/comment/deleteComment?token=" + getToken();
        String jsonResult = "";
        try{
            jsonResult = api_manager.deleteHttp(url,inputJson);
        }catch (Exception e){
            e.printStackTrace();
        }


        assertTrue(jsonResult.indexOf("Success")>-1);
    }



    public boolean loginResult(String loginResult) {
        JSON_CLASS.Login_res obj_loginResult = null;
        obj_loginResult = new Gson().fromJson(loginResult, new TypeToken<JSON_CLASS.Login_res>() {
        }.getType());
        if (obj_loginResult != null) {
            if (obj_loginResult.Result) {
                token = obj_loginResult.token;
                return true;
            }
        }
        return false;
    }


    public String getToken() {
        JSON_CLASS.PostLogin postItems = new JSON_CLASS.PostLogin();
        postItems.password ="12345678";
        postItems.username = "raymond";

        final String inputJson = new Gson().toJson(postItems);
        final String url = "http://" + domain + "/api/users/login";
        String jsonResult = "";
        try{
            jsonResult = api_manager.postHttp(url,inputJson);
        }catch (Exception e){
            e.printStackTrace();
        }
        JSON_CLASS.Login_res obj_loginResult = null;
        obj_loginResult = new Gson().fromJson(jsonResult, new TypeToken<JSON_CLASS.Login_res>() {
        }.getType());
        if (obj_loginResult != null) {
            if (obj_loginResult.Result) {
                token = obj_loginResult.token;
                return token;
            }
        }
        return null;
    }

    @Override
    public void receiveData(String key, String JSON) {

    }
}
