package com.example.recipebook.List;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.recipebook.Activity.InfoActivity;
import com.example.recipebook.Activity.MainActivity;
import com.example.recipebook.API.JSON_CLASS;
import com.example.recipebook.R;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class RecipeListAdapter extends BaseAdapter {
    private Context mContext;
    private List<RecipeList> recipeLists = new ArrayList<>();
    MainActivity mActivity;
    TextView tv_recipe_like_text;

    public RecipeListAdapter(MainActivity mActivity, List<RecipeList> recipeLists) {
        this.mActivity = mActivity;
        this.mContext = mActivity.getApplicationContext();
        this.recipeLists = recipeLists;
    }


    @Override
    public int getCount() {
        return recipeLists.size();
    }

    @Override
    public Object getItem(int position) {
        return recipeLists.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = View.inflate(mContext, R.layout.recipe_list, null);

        ImageView iv_recipelist = v.findViewById(R.id.iv_recipelist);
        TextView tv_recipe_title = v.findViewById(R.id.tv_recipe_title);
        final LinearLayout btn_recipe_like = v.findViewById(R.id.btn_recipe_like);
        tv_recipe_like_text = v.findViewById(R.id.tv_recipe_like_text);

        Glide.with(mActivity)
                .load(recipeLists.get(position).getImage_path())
                .into(iv_recipelist);

        tv_recipe_title.setText(recipeLists.get(position).getTitle_eng());

        if (mActivity.globalVar.language.equals("zh")){
            if (recipeLists.get(position).getTitle_chi() !=null && recipeLists.get(position).getTitle_chi().length()>0){
                tv_recipe_title.setText(recipeLists.get(position).getTitle_chi());
            }
        }

        if (recipeLists.get(position).getLiked()){
            tv_recipe_like_text.setText(mActivity.getResources().getString(R.string.liked));
        }else{
            tv_recipe_like_text.setText(mActivity.getResources().getString(R.string.like));
        }

        btn_recipe_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!recipeLists.get(position).getLiked()){
                    addFavourite(recipeLists.get(position).getId());
                    mActivity.showRecipeLists.get(position).setLiked(true);
                    btn_recipe_like.setEnabled(false);
                }else{
                    delFavourite(recipeLists.get(position).getId());
                    mActivity.showRecipeLists.get(position).setLiked(false);
                    btn_recipe_like.setEnabled(false);
                }
                Animation shake = AnimationUtils.loadAnimation(mActivity, R.anim.shake);
                btn_recipe_like.startAnimation(shake);
            }
        });

        iv_recipelist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mActivity, InfoActivity.class);
                intent.putExtra("id", recipeLists.get(position).getId());
                mActivity.startActivity(intent);
            }
        });

        return v;
    }


    public void addFavourite(int id){
        JSON_CLASS.add_delFavourite postItems = new JSON_CLASS.add_delFavourite();
        postItems.recipeId = id;

        final String inputJson = new Gson().toJson(postItems);

        String url = "http://" + mActivity.globalVar.domain  + "/api/favourite/createFavourite?token=" + mActivity.globalVar.accessToken;
        mActivity.api_manager.postAPI(mActivity.addFavouriteRecipe_key,url,inputJson);
    }

    public void delFavourite(int id){
        JSON_CLASS.add_delFavourite postItems = new JSON_CLASS.add_delFavourite();
        postItems.recipeId = id;

        final String inputJson = new Gson().toJson(postItems);

        String url = "http://" + mActivity.globalVar.domain  + "/api/favourite/deleteFavourite?token=" + mActivity.globalVar.accessToken;
        mActivity.api_manager.delAPI(mActivity.delFavouriteRecipe_key,url,inputJson);
    }
}
