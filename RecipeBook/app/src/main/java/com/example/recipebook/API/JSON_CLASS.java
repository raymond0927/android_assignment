package com.example.recipebook.API;

import com.example.recipebook.List.CommentList;
import com.example.recipebook.List.RecipeList;

import java.util.ArrayList;
import java.util.List;

public class JSON_CLASS {

    public static class PostLogin {
        public String username;
        public String password;
    }

    public static class PostRegister {
        public String username;
        public String password;
        public String email;
    }

    public static class PutUser {
        public String username;
        public String newPassword;
        public String password;
        public String newEmail;
    }

    public static class PostRecipe {
        public String title_chi;
        public String title_eng;
        public String ingredient_chi;
        public String ingredient_eng;
        public String methods_chi;
        public String methods_eng;
    }


    public static class add_delFavourite {
        public int recipeId;
    }

    public static class PutRecipe {
        public int recipeId;
        public String title_chi;
        public String title_eng;
        public String ingredient_chi;
        public String ingredient_eng;
        public String methods_chi;
        public String methods_eng;
    }

    public static class DelImage {
        public List<Integer> imageId;
    }

    public static class DelRecipe {
        public int recipeId;
    }

    public static class PostComment {
        public int recipeId;
        public String comment_eng;
        public String comment_chi;
    }

    public static class DelComment {
        public int commentid;
    }


    // response

    public static class Login_res {
        public Boolean Result;
        public String message;
        public String token;
        public int userId;
    }


    public static class Register_res {
        public Boolean Result;
        public String message;
    }

    public static class getFaouriteList_res {
        public Boolean Result;
        public List<FavouriteMessage> message ;
    }

    public static class FavouriteMessage {
        public int id;
        public int user_id;
        public int recipe_id;
        public String created_date;
        public String modified_date;
    }

    public static class getAllList_res {
        public Boolean Result;
        public List<RecipeList> message ;
    }


    public static class postRecipe_res {
        public Boolean Result ;
        public String message;
        public int id;
    }

    public static class postImageInfo_res {
        public Boolean Result ;
        public String message;
    }

    public static class fail_res {
        public Boolean Result;
        public String message;
    }

    public static class post_res {
        public Boolean Result ;
        public String message;
    }

    public static class getRecipeByid {
        public Boolean Result ;
        public List<recipeInfo> message;
    }

    public static class recipeInfo {
        public int id;
        public int user_id;
        public String author;
        public String title_chi;
        public String title_eng;
        public String ingredient_chi;
        public String ingredient_eng;
        public String methods_chi;
        public String methods_eng;
        public String created_date;
        public String modified_date;
    }

    public static class getCommentByid {
        public Boolean Result ;
        public List<CommentList> message;
    }

    public static class getImageByRecipeid {
        public Boolean Result ;
        public List<ImageInfo> message;
    }


    public static class ImageInfo {
        public int id;
        public int recipe_id;
        public String image_path;
        public String created_date;
        public String modified_date;
    }

}
