package com.example.recipebook.Activity.Fragment;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.recipebook.Activity.InfoActivity;
import com.example.recipebook.Activity.LoginActivity;
import com.example.recipebook.API.CallBackClass;
import com.example.recipebook.API.JSON_CLASS;
import com.example.recipebook.R;
import com.example.recipebook.SliderViewPageAdapter;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

public class Fragment_Recipe extends Fragment {

    private final String LOG_TAG = this.getClass().getName();
    private InfoActivity mActivity;
    ViewPager viewPager;
    SliderViewPageAdapter adapter;
    LinearLayout sliderDots;
    private int dotCounts;
    private ImageView[] dots;

    public Handler aiHandler;
    final public String getRecipeByid_key = "getRecipeByid";
    final public String getImageByid_key = "getImageByid";

    List<JSON_CLASS.ImageInfo> imageInfoList = new ArrayList<>();
    List<String> imageList = new ArrayList<>();

    TextView tv_recipe_info_name , tv_content_ingredients, tv_content_methods;
    ImageView iv_remove_recipe ;
    final String delRecipe_key = "delRecipe";


    public Fragment_Recipe() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fg_recipe, container, false);
        mActivity = InfoActivity.getInstance();
        sliderDots = view.findViewById(R.id.SliderDots);
        viewPager = view.findViewById(R.id.viewPager);
        tv_recipe_info_name =  view.findViewById(R.id.tv_recipe_info_name);
        tv_content_ingredients =  view.findViewById(R.id.tv_content_ingredients);
        tv_content_methods =  view.findViewById(R.id.tv_content_methods);
        iv_remove_recipe = view.findViewById(R.id.iv_remove_recipe);


        iv_remove_recipe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(mActivity)
                        .setTitle(mActivity.getString(R.string.delete_recipe))
                        .setMessage(mActivity.getString(R.string.delete_recipe)+"?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                mActivity.globalVar.showloadingDialog(mActivity,getResources().getString(R.string.delete_recipe));
                                delRecipe(mActivity.recipe_id);
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        });
        aiHandler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message inputMessage) {
                try {
                    CallBackClass result = (CallBackClass) inputMessage.obj;
                    Log.e(LOG_TAG, "AiHandler handle message (" + result.object + "):" + result.result);
                    switch(result.object) {
                        case getRecipeByid_key:
                            getRecipeByid(result.result);
                            break;
                        case getImageByid_key:
                            getImageByRecipeid(result.result);
                            break;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        };


        return view;
    }

    private void getRecipeByid(String result) {
        JSON_CLASS.getRecipeByid obj_recipe_Result = null;
        try{
            obj_recipe_Result = new Gson().fromJson(result, new TypeToken<JSON_CLASS.getRecipeByid>() {}.getType());
        }catch (Exception e){
            e.printStackTrace();
        }
        if (obj_recipe_Result != null) {
            if (obj_recipe_Result.Result) {
                final String method =  obj_recipe_Result.message.get(0).methods_eng;
                final String ingredients =  obj_recipe_Result.message.get(0).ingredient_eng;
                final String name =  obj_recipe_Result.message.get(0).title_eng;
                final String method_chi =  obj_recipe_Result.message.get(0).methods_chi;
                final String ingredients_chi =  obj_recipe_Result.message.get(0).ingredient_chi;
                final String name_chi =  obj_recipe_Result.message.get(0).title_chi;
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        tv_content_methods.setText(method);
                        tv_content_ingredients.setText(ingredients);
                        tv_recipe_info_name.setText(name);
                        if (mActivity.globalVar.language.equals("zh")){
                            if (method_chi!=null &&method_chi.length()>0){
                                tv_content_methods.setText(method_chi);
                            }

                            if (ingredients_chi!=null && ingredients_chi.length()>0){
                                tv_content_ingredients.setText(ingredients_chi);
                            }

                            if (name_chi!=null &&name_chi.length()>0){
                                tv_recipe_info_name.setText(name_chi);
                            }
                        }
                    }
                });

            }
        }else{
            JSON_CLASS.fail_res obj_fail = new Gson().fromJson(result, new TypeToken<JSON_CLASS.fail_res>() {
            }.getType());
            if (obj_fail.message.equals("Auth failed")) {
                mActivity.globalVar.toast(mActivity, mActivity.getResources().getString(R.string.login_expired));
                Intent intent = new Intent(mActivity, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            } else {
                mActivity.globalVar.toast(mActivity, obj_fail.message);
            }

        }
    }

    private void getImageByRecipeid(String result) {
        JSON_CLASS.getImageByRecipeid obj_image_Result = null;
        try{
            obj_image_Result = new Gson().fromJson(result, new TypeToken<JSON_CLASS.getImageByRecipeid>() {
            }.getType());
        }catch (Exception e){
            e.printStackTrace();
        }
        if (obj_image_Result != null) {
            if (obj_image_Result.Result) {
                imageInfoList.clear();
                imageInfoList.addAll(obj_image_Result.message);
                imageList.clear();
                for (int i =0; i<imageInfoList.size();i++){
                    imageList.add(imageInfoList.get(i).image_path);
                }

                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        setImageList(imageList);
                    }
                });
            }
        }else{
            JSON_CLASS.fail_res obj_fail = new Gson().fromJson(result, new TypeToken<JSON_CLASS.fail_res>() {
            }.getType());
            if (obj_fail.message.equals("Auth failed")) {
                mActivity.globalVar.toast(mActivity, mActivity.getResources().getString(R.string.login_expired));
                Intent intent = new Intent(mActivity, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            } else {
                mActivity.globalVar.toast(mActivity, obj_fail.message);
            }

        }
    }

    public void setImageList(List<String> images ){
        Log.v("image size ",images.size()+"");
        adapter = new SliderViewPageAdapter(mActivity,images);
        viewPager.setAdapter(adapter);
        dotCounts = 0;
        sliderDots.removeAllViews();
        dotCounts = adapter.getCount();
        dots = new ImageView[dotCounts];
        for (int i = 0; i < dotCounts; i++) {
            dots[i] = new ImageView(mActivity);
            dots[i].setImageDrawable(ContextCompat.getDrawable(mActivity, R.drawable.slideshow_nonactive_dot));
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(8, 2, 8, 0);
            sliderDots.addView(dots[i], params);
        }
        dots[0].setImageDrawable(ContextCompat.getDrawable(mActivity, R.drawable.slideshow_dot));
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                for (int i = 0; i < dotCounts; i++) {
                    dots[i].setImageDrawable(ContextCompat.getDrawable(mActivity, R.drawable.slideshow_nonactive_dot));
                }
                dots[position].setImageDrawable(ContextCompat.getDrawable(mActivity, R.drawable.slideshow_dot));
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

    }

    public void delRecipe(int recipeId){
        JSON_CLASS.DelRecipe delRecipeItems = new JSON_CLASS.DelRecipe();
        delRecipeItems.recipeId = recipeId;

        final String inputJson = new Gson().toJson(delRecipeItems);

        String url = "http://" +mActivity.globalVar.domain + "/api/recipe/deleteRecipe?token=" + mActivity.globalVar.accessToken;
        mActivity.api_manager.delAPI(delRecipe_key,url,inputJson);
    }

}
