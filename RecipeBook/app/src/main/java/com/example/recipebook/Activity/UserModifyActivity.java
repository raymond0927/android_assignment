package com.example.recipebook.Activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.example.recipebook.API.APIInterface;
import com.example.recipebook.API.API_Manager;
import com.example.recipebook.GlobalVar;
import com.example.recipebook.API.JSON_CLASS;
import com.example.recipebook.R;
import com.example.recipebook.ShakeListener;
import com.example.recipebook.SharedPreferenceManager;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class UserModifyActivity extends AppCompatActivity implements APIInterface {

    private final String LOG_TAG = this.getClass().getName();
    public UserModifyActivity mActivity;
    private GlobalVar globalVar;
    private API_Manager api_manager;
    SharedPreferenceManager mSharedPreferenceManager;

    EditText edt_password_modify , edt_new_password_modify, edt_confirm_password_modify,edt_new_email_modify;
    Switch swh_touchIDLogin;
    LinearLayout ll_mainpage_userEdit;
    TextView tv_userName_modify;

    Button btn_modify_user_confirm;
    ImageView btn_user_modify_back;

    // Shake
    private ShakeListener mShaker;
    private boolean isShakeAlert = false;

    final String putuser_key = "putuser_api";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_modify);

        mActivity = this;
        globalVar = (GlobalVar) getApplicationContext();
        api_manager = new API_Manager(mActivity, mActivity, globalVar);
        SharedPreferenceManager.init(this);
        mSharedPreferenceManager = SharedPreferenceManager.getInstance();
        Log.v(LOG_TAG, mSharedPreferenceManager.getLanguage().toString());

        bindViews();
        setListeners();

        mShaker = new ShakeListener(this);
        mShaker.setOnShakeListener(new ShakeListener.OnShakeListener() {
            public void onShake() {
                if (!isShakeAlert) {
                    isShakeAlert = true;
                    new AlertDialog.Builder(mActivity)
                            .setTitle(mActivity.getString(R.string.clear_input))
                            .setMessage(mActivity.getString(R.string.confirm_clear_input))
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    clearAll();
                                    isShakeAlert = false;
                                }
                            })
                            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // do nothing
                                    isShakeAlert = false;
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                }
            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mShaker.pause();
    }


    @Override
    public void receiveData(String key, String JSON) {
        switch (key) {
            case putuser_key:
                globalVar.dismissloadingDialog(mActivity);
                putRecipeResult(JSON);
                break;

        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    private void bindViews() {
        edt_password_modify = findViewById(R.id.edt_password_modify);
        edt_new_password_modify = findViewById(R.id.edt_new_password_modify);
        edt_confirm_password_modify = findViewById(R.id.edt_confirm_password_modify);
        edt_new_email_modify = findViewById(R.id.edt_new_email_modify);
        btn_modify_user_confirm = findViewById(R.id.btn_modify_user_confirm);
        btn_user_modify_back = findViewById(R.id.btn_user_modify_back);
        tv_userName_modify = findViewById(R.id.tv_userName_modify);
        ll_mainpage_userEdit = findViewById(R.id.ll_mainpage_userEdit);
        swh_touchIDLogin = findViewById(R.id.swh_touchIDLogin);

        tv_userName_modify.setText(globalVar.userName);

        Animation anim = AnimationUtils.loadAnimation(this, R.anim.frombottom);
        anim.setInterpolator((new AccelerateDecelerateInterpolator()));
        anim.setFillAfter(true);
        ll_mainpage_userEdit.setAnimation(anim);
        if (mSharedPreferenceManager.getRemember()){
            swh_touchIDLogin.setChecked(true);
        }

    }

    private void setListeners() {
        btn_modify_user_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (inputchecking()){
                    globalVar.showloadingDialog(mActivity,getResources().getString(R.string.edit_user));
                    putUser();
                }
            }
        });
        btn_user_modify_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        swh_touchIDLogin.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mSharedPreferenceManager.setRemember(true);
                    mSharedPreferenceManager.setPassword(globalVar.password);
                    mSharedPreferenceManager.saveSharedPreference();
                } else {
                    mSharedPreferenceManager.setRemember(false);
                    mSharedPreferenceManager.setPassword("");
                    mSharedPreferenceManager.saveSharedPreference();
                }
            }
        });
    }


    private void clearAll() {
        edt_password_modify.setText("");
        edt_new_password_modify.setText("");
        edt_confirm_password_modify.setText("");
        edt_new_email_modify.setText("");
    }

    private Boolean inputchecking(){
        if (edt_password_modify.getText().toString().length() ==0){
            globalVar.toast(mActivity,getResources().getString(R.string.password_empty));
            return false;
        }
        if (edt_new_password_modify.getText().toString().length() ==0 &&edt_new_email_modify.getText().toString().length() ==0  ){
            globalVar.toast(mActivity,getResources().getString(R.string.new_email_or_newpassword_empty));
            return false;
        }
        if (!edt_confirm_password_modify.getText().toString().equals(edt_new_password_modify.getText().toString())){
            globalVar.toast(mActivity,getResources().getString(R.string.password_not_match));
            return false;
        }

        return true;
    }


    public void putUser(){
        JSON_CLASS.PutUser putItems = new JSON_CLASS.PutUser();
        putItems.username = globalVar.userName;
        putItems.password = edt_password_modify.getText().toString();
        putItems.newEmail = edt_new_email_modify.getText().toString();
        putItems.newPassword = edt_new_password_modify.getText().toString();


        final String inputJson = new Gson().toJson(putItems);

        String url = "http://" +globalVar.domain + "/api/users/modifyPassword" ;
        api_manager.putAPI(putuser_key,url,inputJson);
    }

    private void putRecipeResult(String result) {
        JSON_CLASS.post_res obj_put_Result = null;
        obj_put_Result = new Gson().fromJson(result, new TypeToken<JSON_CLASS.post_res>() {
        }.getType());
        if (obj_put_Result != null) {
            if (obj_put_Result.Result) {
                globalVar.toast(mActivity, obj_put_Result.message);
                if (mSharedPreferenceManager.getRemember()){
                    mSharedPreferenceManager.setPassword(edt_new_password_modify.getText().toString());
                    mSharedPreferenceManager.saveSharedPreference();
                }
                finish();
            } else {
                if (obj_put_Result.message.equals("Auth failed")) {
                    globalVar.toast(mActivity, mActivity.getResources().getString(R.string.login_expired));
                    Intent intent = new Intent(mActivity, LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                } else {
                    globalVar.toast(mActivity, obj_put_Result.message);
                }
                globalVar.dismissloadingDialog(mActivity);
            }
        }
    }
}
