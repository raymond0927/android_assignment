package com.example.recipebook.List;

public class RecipeList {
    private int id;
    private int user_id;
    private String title_chi;
    private String title_eng;
    private String image_path;
    private Boolean isLiked = false;

    public RecipeList(int id, int user_id, String title_chi, String title_eng, String image_path) {
        this.id = id;
        this.user_id = user_id;
        this.title_chi = title_chi;
        this.title_eng = title_eng;
        this.image_path = image_path;
        isLiked = false;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getTitle_chi() {
        return title_chi;
    }

    public void setTitle_chi(String title_chi) {
        this.title_chi = title_chi;
    }

    public String getTitle_eng() {
        return title_eng;
    }

    public void setTitle_eng(String title_eng) {
        this.title_eng = title_eng;
    }

    public String getImage_path() {
        return image_path;
    }

    public void setImage_path(String image_path) {
        this.image_path = image_path;
    }

    public Boolean getLiked() {
        return isLiked;
    }

    public void setLiked(Boolean liked) {
        isLiked = liked;
    }
}
