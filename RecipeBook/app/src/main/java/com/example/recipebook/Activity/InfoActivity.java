package com.example.recipebook.Activity;

import android.app.Fragment;
import android.content.Intent;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.example.recipebook.API.APIInterface;
import com.example.recipebook.API.API_Manager;
import com.example.recipebook.Activity.Fragment.Fragment_Comment;
import com.example.recipebook.Activity.Fragment.Fragment_Recipe;
import com.example.recipebook.API.CallBackClass;
import com.example.recipebook.GlobalVar;
import com.example.recipebook.API.JSON_CLASS;
import com.example.recipebook.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class InfoActivity extends AppCompatActivity implements APIInterface {


    private final String LOG_TAG = this.getClass().getName();
    public GlobalVar globalVar;
    public API_Manager api_manager;

    private ImageView iv_recipe, iv_comment, btn_info_back, btn_info_add;
    private LinearLayout ll_recipe, ll_comment;
    private RelativeLayout rl_fragment_container;

    private static InfoActivity mActivity;
    public Fragment_Recipe fg_Recipe = new Fragment_Recipe();
    public Fragment_Comment fg_Comment = new Fragment_Comment();
    private Fragment current_fragment;
    boolean isRecipe = true;

    String recipe_info_json = "";
    String recipe_image_json = "";


    public int recipe_id = -1;


    final String getComment_key = "getComment";
    final String getRecipeByid_key = "getRecipeByid";
    final String getImageByid_key = "getImageByid";
    final String delRecipe_key = "delRecipe";


    public static InfoActivity getInstance() {
        return mActivity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);
        mActivity = this;
        bindViews();
        setListeners();
        goToFragment(fg_Recipe);
        Bundle extras = getIntent().getExtras();
        recipe_id = extras.getInt("id");

    }

    @Override
    protected void onResume() {
        super.onResume();
        globalVar = (GlobalVar) getApplicationContext();
        mActivity = this;
        api_manager = new API_Manager(mActivity, mActivity, globalVar);
        if (isRecipe) {
            String url = "http://" + globalVar.domain + "/api/recipe/getRecipeById?token=" + globalVar.accessToken + "&recipeId=" + recipe_id;
            api_manager.getAPI(getRecipeByid_key, url);
            String url_image = "http://" + globalVar.domain + "/api/recipe_image/getImageByRecipeid?token=" + globalVar.accessToken + "&recipeId=" + recipe_id;
            api_manager.getAPI(getImageByid_key, url_image);
        } else {
            String url = "http://" + globalVar.domain + "/api/comment/getCommentByRecipeId?token=" + globalVar.accessToken + "&recipeId=" + recipe_id;
            Log.v("url comment ", url);
            api_manager.getAPI(getComment_key, url);
        }
    }


    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void receiveData(String key, String JSON) {
        switch (key) {
            case getComment_key:
                Message msg = new Message();
                msg.obj = new CallBackClass(getComment_key, JSON);
                fg_Comment.aiHandler.sendMessage(msg);
                break;
            case getRecipeByid_key:
                Message msg_recipe = new Message();
                msg_recipe.obj = new CallBackClass(getRecipeByid_key, JSON);
                fg_Recipe.aiHandler.sendMessage(msg_recipe);
                recipe_info_json = JSON;
                getRecipeByid(JSON);
                break;
            case getImageByid_key:
                Message msg_image = new Message();
                msg_image.obj = new CallBackClass(getImageByid_key, JSON);
                fg_Recipe.aiHandler.sendMessage(msg_image);
                recipe_image_json = JSON;
                break;
            case delRecipe_key:
                globalVar.dismissloadingDialog(mActivity);
                delRecipeResult(JSON);
                break;
        }
    }


    private void bindViews() {
        iv_recipe = findViewById(R.id.iv_recipe);
        iv_comment = findViewById(R.id.iv_comment);
        ll_recipe = findViewById(R.id.ll_recipe);
        ll_comment = findViewById(R.id.ll_comment);
        rl_fragment_container = findViewById(R.id.rl_fragment_container);
        btn_info_back = findViewById(R.id.btn_info_back);
        btn_info_add = findViewById(R.id.btn_info_add);

        iv_recipe.setSelected(true);
    }

    private void setListeners() {
        ll_recipe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSelected();
                btn_info_add.setImageResource(R.mipmap.ic_edit);
                iv_recipe.setSelected(true);
                goToFragment(fg_Recipe);
                isRecipe = true;
                String url = "http://" + globalVar.domain + "/api/recipe/getRecipeById?token=" + globalVar.accessToken + "&recipeId=" + recipe_id;
                api_manager.getAPI(getRecipeByid_key, url);
                String url_image = "http://" + globalVar.domain + "/api/recipe_image/getImageByRecipeid?token=" + globalVar.accessToken + "&recipeId=" + recipe_id;
                api_manager.getAPI(getImageByid_key, url_image);
            }
        });
        ll_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSelected();
                btn_info_add.setVisibility(View.VISIBLE);
                btn_info_add.setImageResource(R.mipmap.ic_plus);
                iv_comment.setSelected(true);
                goToFragment(fg_Comment);
                isRecipe = false;
                String url = "http://" + globalVar.domain + "/api/comment/getCommentByRecipeId?token=" + globalVar.accessToken + "&recipeId=" + recipe_id;
                api_manager.getAPI(getComment_key, url);
            }
        });
        btn_info_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btn_info_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isRecipe) {
                    Intent intent = new Intent(mActivity, CommentActivity.class);
                    intent.putExtra("recipe_id", recipe_id);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(mActivity, RecipeEditActivity.class);
                    intent.putExtra("isEdit", true);
                    intent.putExtra("recipe_info_json", recipe_info_json);
                    intent.putExtra("recipe_image_json", recipe_image_json);
                    startActivity(intent);
                }
            }
        });

    }

    //重置所有文本的选中状态
    private void setSelected() {
        iv_recipe.setSelected(false);
        iv_comment.setSelected(false);
    }


    public void goToFragment(final Fragment fragment) {
        if (rl_fragment_container != null && current_fragment != fragment) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    rl_fragment_container.setVisibility(View.VISIBLE);
                    current_fragment = fragment;
                    getFragmentManager().beginTransaction().replace(R.id.rl_fragment_container, fragment).addToBackStack(null).commit();
                }
            });
        }
    }

    public void clearFragment() {
        Log.i(LOG_TAG, "CLEAR FRAGMENT");
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                rl_fragment_container.removeAllViews();
                rl_fragment_container.setVisibility(View.GONE);
                current_fragment = null;
            }
        });
    }

    private void getRecipeByid(String result) {
        JSON_CLASS.getRecipeByid obj_recipe_Result = null;
        obj_recipe_Result = new Gson().fromJson(result, new TypeToken<JSON_CLASS.getRecipeByid>() {
        }.getType());
        if (obj_recipe_Result != null) {
            if (obj_recipe_Result.Result) {
                final int recipe_userId = obj_recipe_Result.message.get(0).user_id;
                Log.v("hihiuserID", recipe_userId + "  " + globalVar.userId);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (recipe_userId == globalVar.userId) {
                            btn_info_add.setVisibility(View.VISIBLE);
                        } else {
                            btn_info_add.setVisibility(View.GONE);
                        }
                    }
                });

            }
        }
    }

    private void delRecipeResult(String result) {
        JSON_CLASS.post_res obj_del_Result = null;
        try {
            obj_del_Result = new Gson().fromJson(result, new TypeToken<JSON_CLASS.post_res>() {
            }.getType());
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (obj_del_Result != null) {
            if (obj_del_Result.Result) {
                finish();
            } else {
                if (obj_del_Result.message.equals("Auth failed")) {
                    globalVar.toast(mActivity, mActivity.getResources().getString(R.string.login_expired));
                } else {
                    globalVar.toast(mActivity, obj_del_Result.message);
                }
            }
        } else {
            if (obj_del_Result.message.equals("Auth failed")) {
                globalVar.toast(mActivity, mActivity.getResources().getString(R.string.login_expired));
            } else {
                globalVar.toast(mActivity, obj_del_Result.message);
            }
        }
    }

}
