package com.example.recipebook.Activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.example.recipebook.API.APIInterface;
import com.example.recipebook.API.API_Manager;
import com.example.recipebook.GlobalVar;
import com.example.recipebook.API.JSON_CLASS;
import com.example.recipebook.R;
import com.example.recipebook.List.RecipeList;
import com.example.recipebook.List.RecipeListAdapter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements APIInterface {

    private final String LOG_TAG = this.getClass().getName();
    public GlobalVar globalVar;
    public API_Manager api_manager;

    private ImageView iv_home, iv_favourite, iv_add, iv_member, iv_setting, iv_logout;

    private RelativeLayout rl_mainpage;
    public List<RecipeList> showRecipeLists = new ArrayList<>();
    List<JSON_CLASS.FavouriteMessage> favouriteList = new ArrayList<>();
    private RecipeListAdapter adapter;
    private ListView lv_recipe;
    MainActivity mActivity;

    final String getAllRecipe_key = "getAllRecipe";
    final String getUserRecipe_key = "getUserRecipe";
    final String getFavouriteRecipe_key = "getFavouriteRecipe";
    final public String addFavouriteRecipe_key = "addFavouriteRecipe";
    final public String delFavouriteRecipe_key = "delFavouriteRecipe";

    Boolean isgetAll = true;
    Boolean isgetUser = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bindViews();
        setListeners();

        Animation anim = AnimationUtils.loadAnimation(this, R.anim.frombottom);
        anim.setInterpolator((new AccelerateDecelerateInterpolator()));
        anim.setFillAfter(true);
        rl_mainpage.setAnimation(anim);

        adapter = new RecipeListAdapter(this, showRecipeLists);
        lv_recipe.setAdapter(adapter);

    }

    @Override
    protected void onResume() {
        super.onResume();
        globalVar = (GlobalVar) getApplicationContext();
        mActivity = this;
        api_manager = new API_Manager(mActivity, mActivity, globalVar);

        String url = "http://" + globalVar.domain + "/api/favourite/getFavouriteListByUserID?token=" + globalVar.accessToken;
        api_manager.getAPI(getFavouriteRecipe_key, url);
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(mActivity)
                .setTitle(mActivity.getString(R.string.quit))
                .setMessage(mActivity.getString(R.string.confirm_quit))
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        globalVar.accessToken = "";
                        globalVar.userId = -1;
                        globalVar.userName = "";
                        Intent intent = new Intent(mActivity, LoginActivity.class);
                        startActivity(intent);
                        finish();
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    @Override
    public void receiveData(String key, String JSON) {
        switch (key) {
            case getFavouriteRecipe_key:
                getFavouriteResult(JSON);
                if (isgetAll) {
                    String url = "http://" +globalVar.domain  + "/api/recipe/getAllRecipe?token=" + globalVar.accessToken;
                    api_manager.getAPI(getAllRecipe_key, url);
                } else if (isgetUser) {
                    String url = "http://" + globalVar.domain  + "/api/recipe/getRecipeByUserId?token=" + globalVar.accessToken;
                    api_manager.getAPI(getUserRecipe_key, url);
                } else {
                    String url = "http://" +globalVar.domain  + "/api/recipe/getAllRecipe?token=" + globalVar.accessToken;
                    api_manager.getAPI(getAllRecipe_key, url);
                }
                break;
            case getAllRecipe_key:
                if (isgetAll) {
                    globalVar.dismissloadingDialog(mActivity);
                    getRecipeResult(JSON);
                } else {
                    globalVar.dismissloadingDialog(mActivity);
                    getFavouriteListResult(JSON);
                }
                break;
            case getUserRecipe_key:
                globalVar.dismissloadingDialog(mActivity);
                getUserResult(JSON) ;
                break;
            case addFavouriteRecipe_key:
                add_delFavouriteResult(JSON);
                break;
            case delFavouriteRecipe_key:
                add_delFavouriteResult(JSON);
                break;

        }
    }

    private void bindViews() {
        iv_home = findViewById(R.id.iv_home);
        iv_favourite = findViewById(R.id.iv_favourite);
        iv_add = findViewById(R.id.iv_add);
        iv_member = findViewById(R.id.iv_member);
        iv_setting = findViewById(R.id.iv_setting);
        rl_mainpage = findViewById(R.id.rl_mainpage);
        lv_recipe = findViewById(R.id.lv_recipe);
        iv_logout = findViewById(R.id.iv_logout);

        iv_home.setSelected(true);
        iv_home.setEnabled(false);
    }

    private void setListeners() {
        iv_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                globalVar.showloadingDialog(mActivity,getResources().getString(R.string.getdata));
                setSelected();
                iv_home.setSelected(true);
                iv_home.setEnabled(false);
                isgetAll = true;
                isgetUser = false;
                String url = "http://" +globalVar.domain + "/api/favourite/getFavouriteListByUserID?token=" + globalVar.accessToken;
                api_manager.getAPI(getFavouriteRecipe_key, url);
            }
        });
        iv_favourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                globalVar.showloadingDialog(mActivity,getResources().getString(R.string.getdata));
                setSelected();
                iv_favourite.setSelected(true);
                iv_favourite.setEnabled(false);
                isgetAll = false;
                isgetUser = false;
                String url = "http://" + globalVar.domain  + "/api/favourite/getFavouriteListByUserID?token=" + globalVar.accessToken;
                api_manager.getAPI(getFavouriteRecipe_key, url);
            }
        });

        iv_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mActivity, RecipeEditActivity.class);
                intent.putExtra("isEdit", false);
                mActivity.startActivity(intent);
            }
        });

        iv_member.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                globalVar.showloadingDialog(mActivity,getResources().getString(R.string.getdata));
                setSelected();
                iv_member.setSelected(true);
                iv_member.setEnabled(false);
                isgetAll = false;
                isgetUser = true;
                String url = "http://" + globalVar.domain + "/api/favourite/getFavouriteListByUserID?token=" + globalVar.accessToken;
                api_manager.getAPI(getFavouriteRecipe_key, url);
            }
        });

        iv_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mActivity, UserModifyActivity.class);
                mActivity.startActivity(intent);
            }
        });

        iv_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(mActivity)
                        .setTitle(mActivity.getString(R.string.quit))
                        .setMessage(mActivity.getString(R.string.confirm_quit))
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                globalVar.accessToken = "";
                                globalVar.userId = -1;
                                globalVar.userName = "";
                                Intent intent = new Intent(mActivity, LoginActivity.class);
                                startActivity(intent);
                                finish();
                                finish();
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        });
    }

    //重置所有文本的选中状态
    private void setSelected() {
        iv_home.setSelected(false);
        iv_favourite.setSelected(false);
        iv_add.setSelected(false);
        iv_member.setSelected(false);
        iv_setting.setSelected(false);
        iv_home.setEnabled(true);
        iv_favourite.setEnabled(true);
        iv_add.setEnabled(true);
        iv_member.setEnabled(true);
        iv_setting.setEnabled(true);
    }

    private void getFavouriteResult(String result) {
        Log.v(LOG_TAG + " favourite +", result);
        JSON_CLASS.getFaouriteList_res obj_favourite_Result = null;
        try{
            obj_favourite_Result = new Gson().fromJson(result, new TypeToken<JSON_CLASS.getFaouriteList_res>() {
            }.getType());
        }catch (Exception e){
            e.printStackTrace();
        }

        if (obj_favourite_Result != null) {
            if (obj_favourite_Result.Result) {
                favouriteList = obj_favourite_Result.message;
            } else {
                JSON_CLASS.fail_res obj_fail = new Gson().fromJson(result, new TypeToken<JSON_CLASS.fail_res>() {
                }.getType());
                if (obj_fail.message.equals("Auth failed")) {
                    globalVar.toast(mActivity, mActivity.getResources().getString(R.string.login_expired));
                    Intent intent = new Intent(mActivity, LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                } else {
                    globalVar.toast(mActivity, obj_fail.message);
                }

            }
        }else {
            JSON_CLASS.fail_res obj_fail = new Gson().fromJson(result, new TypeToken<JSON_CLASS.fail_res>() {
            }.getType());
            if (obj_fail != null) {
                if (obj_fail.message.equals("Auth failed")) {
                    mActivity.globalVar.toast(mActivity, mActivity.getResources().getString(R.string.login_expired));
                    Intent intent = new Intent(mActivity, LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                } else {
                    mActivity.globalVar.toast(mActivity, obj_fail.message);
                }
            }
        }
    }

    private void getRecipeResult(String result) {
        JSON_CLASS.getAllList_res obj_all_Result = null;
        try{
            obj_all_Result = new Gson().fromJson(result, new TypeToken<JSON_CLASS.getAllList_res>() {
            }.getType());
        }catch (Exception e){
            e.printStackTrace();
        }
        if (obj_all_Result != null) {
            if (obj_all_Result.Result) {
                List<RecipeList> allList = obj_all_Result.message;

                for (int i = 0; i < allList.size(); i++) {
                    allList.get(i).setLiked(false);
                    for (int y = 0; y < favouriteList.size(); y++) {
                        if (favouriteList.get(y).recipe_id == allList.get(i).getId()) {
                            allList.get(i).setLiked(true);
                            break;
                        }
                    }
                }
                showRecipeLists.clear();
                showRecipeLists.addAll(allList);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        adapter.notifyDataSetChanged();
                    }
                });

            } else {
                JSON_CLASS.fail_res obj_fail = new Gson().fromJson(result, new TypeToken<JSON_CLASS.fail_res>() {
                }.getType());
                if (obj_fail.message.equals("Auth failed")) {
                    globalVar.toast(mActivity, mActivity.getResources().getString(R.string.login_expired));
                    Intent intent = new Intent(mActivity, LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                } else {
                    globalVar.toast(mActivity, obj_fail.message);
                }
            }
        }else{
            JSON_CLASS.fail_res obj_fail = new Gson().fromJson(result, new TypeToken<JSON_CLASS.fail_res>() {
            }.getType());
            if (obj_fail != null) {
                if (obj_fail.message.equals("Auth failed")) {
                    mActivity.globalVar.toast(mActivity, mActivity.getResources().getString(R.string.login_expired));
                    Intent intent = new Intent(mActivity, LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                } else {
                    mActivity.globalVar.toast(mActivity, obj_fail.message);
                }
            }
        }
    }

    private void getFavouriteListResult(String result) {
        JSON_CLASS.getAllList_res obj_all_Result = null;
        try{
            obj_all_Result = new Gson().fromJson(result, new TypeToken<JSON_CLASS.getAllList_res>() {
            }.getType());
        }catch (Exception e){
            e.printStackTrace();
        }
        if (obj_all_Result != null) {
            if (obj_all_Result.Result) {
                List<RecipeList> allList = obj_all_Result.message;
                List<RecipeList> favouriteRecipeList = new ArrayList<>();

                for (int i = 0; i < allList.size(); i++) {
                    for (int y = 0; y < favouriteList.size(); y++) {
                        if (favouriteList.get(y).recipe_id == allList.get(i).getId()) {
                            allList.get(i).setLiked(true);
                            favouriteRecipeList.add( allList.get(i));
                            break;
                        }
                    }
                }
                showRecipeLists.clear();
                showRecipeLists.addAll(favouriteRecipeList);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        adapter.notifyDataSetChanged();
                    }
                });

            } else {
                JSON_CLASS.fail_res obj_fail = new Gson().fromJson(result, new TypeToken<JSON_CLASS.fail_res>() {
                }.getType());
                if (obj_fail.message.equals("Auth failed")) {
                    globalVar.toast(mActivity, mActivity.getResources().getString(R.string.login_expired));
                    Intent intent = new Intent(mActivity, LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                } else {
                    globalVar.toast(mActivity, obj_fail.message);
                }
            }
        }else{
            JSON_CLASS.fail_res obj_fail = new Gson().fromJson(result, new TypeToken<JSON_CLASS.fail_res>() {
            }.getType());
            if (obj_fail != null) {
                if (obj_fail.message.equals("Auth failed")) {
                    mActivity.globalVar.toast(mActivity, mActivity.getResources().getString(R.string.login_expired));
                    Intent intent = new Intent(mActivity, LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                } else {
                    mActivity.globalVar.toast(mActivity, obj_fail.message);
                }
            }
        }
    }

    private void getUserResult(String result) {
        JSON_CLASS.getAllList_res obj_user_Result = null;
        try{
            obj_user_Result = new Gson().fromJson(result, new TypeToken<JSON_CLASS.getAllList_res>() {
            }.getType());
        }catch (Exception e){
            e.printStackTrace();
        }
        if (obj_user_Result != null) {
            if (obj_user_Result.Result) {
                List<RecipeList> userList = obj_user_Result.message;

                for (int i = 0; i < userList.size(); i++) {
                    userList.get(i).setLiked(false);
                    for (int y = 0; y < favouriteList.size(); y++) {
                        if (favouriteList.get(y).recipe_id == userList.get(i).getId()) {
                            userList.get(i).setLiked(true);
                            break;
                        }
                    }
                }
                showRecipeLists.clear();
                showRecipeLists.addAll(userList);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        adapter.notifyDataSetChanged();
                    }
                });

            } else {
                JSON_CLASS.fail_res obj_fail = new Gson().fromJson(result, new TypeToken<JSON_CLASS.fail_res>() {
                }.getType());
                if (obj_fail.message.equals("Auth failed")) {
                    globalVar.toast(mActivity, mActivity.getResources().getString(R.string.login_expired));
                    Intent intent = new Intent(mActivity, LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                } else {
                    globalVar.toast(mActivity, obj_fail.message);
                }
            }
        }else{
            JSON_CLASS.fail_res obj_fail = new Gson().fromJson(result, new TypeToken<JSON_CLASS.fail_res>() {
            }.getType());
            if (obj_fail != null) {
                if (obj_fail.message.equals("Auth failed")) {
                    mActivity.globalVar.toast(mActivity, mActivity.getResources().getString(R.string.login_expired));
                    Intent intent = new Intent(mActivity, LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                } else {
                    mActivity.globalVar.toast(mActivity, obj_fail.message);
                }
            }
        }
    }

    private void add_delFavouriteResult(String result) {
        JSON_CLASS.post_res obj_Result = null;
        try{
            obj_Result = new Gson().fromJson(result, new TypeToken<JSON_CLASS.post_res>() {
            }.getType());
        }catch (Exception e){
            e.printStackTrace();
        }
        if (obj_Result != null) {
            if (obj_Result.Result) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        adapter.notifyDataSetChanged();
                    }
                });
            } else {
                if (obj_Result.message.equals("Auth failed")) {
                    globalVar.toast(mActivity, mActivity.getResources().getString(R.string.login_expired));
                    Intent intent = new Intent(mActivity, LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                } else {
                    globalVar.toast(mActivity, obj_Result.message);
                }
            }
        }else{
            JSON_CLASS.fail_res obj_fail = new Gson().fromJson(result, new TypeToken<JSON_CLASS.fail_res>() {
            }.getType());
            if (obj_fail != null) {
                if (obj_fail.message.equals("Auth failed")) {
                    mActivity.globalVar.toast(mActivity, mActivity.getResources().getString(R.string.login_expired));
                    Intent intent = new Intent(mActivity, LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                } else {
                    mActivity.globalVar.toast(mActivity, obj_fail.message);
                }
            }
        }
    }

}
