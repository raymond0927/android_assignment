package com.example.recipebook.API;

import android.app.Activity;
import android.util.Log;

import com.example.recipebook.GlobalVar;
import com.example.recipebook.R;
import com.google.gson.Gson;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class API_Manager {

    private final String LOG_TAG = this.getClass().getName();
    public static GlobalVar globalVar;
    private static API_Manager api_manager;
    public static APIInterface apiInterface;
    Activity mActivity;


    public API_Manager(APIInterface apiInterface , Activity mActivity, GlobalVar globalVar) {
        if (this.apiInterface!=null){this.apiInterface=null;}
        if (this.mActivity!=null){this.mActivity=null;}
        this.apiInterface=apiInterface;
        this.mActivity = mActivity;
        this.globalVar = globalVar;
        api_manager = this;
    }

    public void getAPI(final String key, final String url){
        Log.v(LOG_TAG+" "+key,url);
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (globalVar.isNetworkAvailable(mActivity)) {
                    String Result = "";
                    try {
                        Result = getHttp(url);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Log.v("GETResult", Result);
                    apiInterface.receiveData(key, Result);
                } else {
                    globalVar.toast(mActivity, mActivity.getResources().getString(R.string.network_connection));
                }
            }
        }).start();
    }

    public void postAPI(final String key, final String url, final String InputJSON){
        Log.v(LOG_TAG+" "+key,url);
        Log.v(LOG_TAG+" InputJSON ",InputJSON);
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (globalVar.isNetworkAvailable(mActivity)) {
                    String Result = "";
                    try {
                        Result = postHttp(url, InputJSON);
                    } catch (IOException e) {
                        e.printStackTrace();
                        JSON_CLASS.fail_res fail_res = new JSON_CLASS.fail_res();
                        fail_res.Result = false;
                        fail_res.message = mActivity.getResources().getString(R.string.connect_timeout);
                        Result = new Gson().toJson(fail_res);
                    }
                    Log.v("POSTResult", Result);
                    apiInterface.receiveData(key, Result);
                } else {
                    globalVar.toast(mActivity, mActivity.getResources().getString(R.string.network_connection));
                }
            }
        }).start();
    }

    public void putAPI(final String key, final String url, final String InputJSON){
        Log.v(LOG_TAG+" "+key,url);
        Log.v(LOG_TAG+" InputJSON ",InputJSON);
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (globalVar.isNetworkAvailable(mActivity)) {
                    String Result = "";
                    try {
                        Result = putHttp(url, InputJSON);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Log.v("PuTResult", Result);
                    apiInterface.receiveData(key, Result);
                } else {
                    globalVar.toast(mActivity, mActivity.getResources().getString(R.string.network_connection));
                }
            }
        }).start();
    }

    public void delAPI(final String key, final String url, final String InputJSON){
        Log.v(LOG_TAG+" "+key,url);
        Log.v(LOG_TAG+" InputJSON ",InputJSON);
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (globalVar.isNetworkAvailable(mActivity)) {
                    String Result = "";
                    try {
                        Result = deleteHttp(url, InputJSON);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Log.v("delResult", Result);
                    apiInterface.receiveData(key, Result);
                } else {
                    globalVar.toast(mActivity, mActivity.getResources().getString(R.string.network_connection));
                }
            }
        }).start();
    }

    public void postImage(final String key, final String url, final List<File> fileList){
        Log.v(LOG_TAG+" "+key,url);
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (globalVar.isNetworkAvailable(mActivity)) {
                    String Result = "";
                    try {
                        Result = postImg(url, fileList);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Log.v("POSTImageResult", Result);
                    apiInterface.receiveData(key, Result);
                } else {
                    globalVar.toast(mActivity, mActivity.getResources().getString(R.string.network_connection));
                }
            }
        }).start();
    }

    public void postVoice(final String key, final String url, final File file){
        Log.v(LOG_TAG+" "+key,url);
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (globalVar.isNetworkAvailable(mActivity)) {
                    String Result = "";
                    try {
                        Result = postVoice(url, file);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Log.v("POSTImageResult", Result);
                    apiInterface.receiveData(key, Result);
                } else {
                    globalVar.toast(mActivity, mActivity.getResources().getString(R.string.network_connection));
                }
            }
        }).start();
    }










    // CRUD


    public String getHttp(String url) throws IOException {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(url)
                .build();

        Response response = client.newCall(request).execute();
        return response.body().string();
    }

    public String postHttp(String url, String json) throws IOException {
        OkHttpClient client = new OkHttpClient();
        final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        RequestBody body = RequestBody.create(JSON, json);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        Response response = client.newCall(request).execute();
        return response.body().string();
    }

    public String putHttp(String url, String jsonBody)  throws IOException {

        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        RequestBody body = RequestBody.create(JSON, jsonBody);

        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(url)
                .put(body) //PUT
                .build();

        Response response = client.newCall(request).execute();
        return response.body().string();
    }

    public String deleteHttp(String url, String jsonBody)  throws IOException {

        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        RequestBody body = RequestBody.create(JSON, jsonBody);

        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(url)
                .delete(body) //Delete
                .build();

        Response response = client.newCall(request).execute();
        return response.body().string();
    }



    //put Image

    private String post(MediaType mediaType, String uploadUrl, List<File> fileList ) throws IOException {
//        File file = new File(localPath);
//        RequestBody body = RequestBody.create(mediaType, file);

        MultipartBody.Builder builder = new MultipartBody.Builder();
        for (int i = 0 ; i < fileList.size() ; i++){
            builder.setType(MultipartBody.FORM).addFormDataPart("image",fileList.get(i).getName(), RequestBody.create(mediaType, fileList.get(i)));
        }
        RequestBody req = builder.build();
        Request request = new Request.Builder()
                .url(uploadUrl)
                .post(req)
                .build();
        //修改各种 Timeout
        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(600, TimeUnit.SECONDS)
                .readTimeout(200, TimeUnit.SECONDS)
                .writeTimeout(600, TimeUnit.SECONDS)
                .build();
        //如果不需要可以直接写成 OkHttpClient client = new OkHttpClient.Builder().build();

        Response response = client
                .newCall(request)
                .execute();
        return response.body().string();
    }

    //上传JPG图片
    private String postImg(String uploadUrl,List<File> fileList) throws IOException {
        MediaType imageType = MediaType.parse("image/jpeg; charset=utf-8");
        return post(imageType, uploadUrl, fileList);
    }

    //post voice

    private String post_audio(MediaType mediaType, String uploadUrl, File file ) throws IOException {
        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM).addFormDataPart("voice",file.getName(), RequestBody.create(mediaType, file));

        RequestBody req = builder.build();

        Request request = new Request.Builder()
                .url(uploadUrl)
                .post(req)
                .build();
        //修改各种 Timeout
        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(600, TimeUnit.SECONDS)
                .readTimeout(200, TimeUnit.SECONDS)
                .writeTimeout(600, TimeUnit.SECONDS)
                .build();
        //如果不需要可以直接写成 OkHttpClient client = new OkHttpClient.Builder().build();

        Response response = client
                .newCall(request)
                .execute();
        return response.body().string();
    }

    //上传JPG图片
    private String postVoice(String uploadUrl,File file ) throws IOException {
        MediaType audioType = MediaType.parse("audio/*; charset=utf-8");
        return post_audio(audioType, uploadUrl, file);
    }


}
