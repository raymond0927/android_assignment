package com.example.recipebook.Activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.example.recipebook.API.APIInterface;
import com.example.recipebook.API.API_Manager;
import com.example.recipebook.GlobalVar;
import com.example.recipebook.API.JSON_CLASS;
import com.example.recipebook.R;
import com.example.recipebook.ShakeListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.io.IOException;

public class CommentActivity extends AppCompatActivity implements APIInterface {

    private final String LOG_TAG = this.getClass().getName();
    public CommentActivity mActivity;
    private EditText edt_comment, edt_comment_chi;
    private TextView tv_comment, tv_comment_chi;
    private Button btn_comment_confirm;
    private ImageView btn_comment_back;
    private LinearLayout ll_mainpage_comment , ll_voice_comment;
    private Switch swh_voice_comment;
    private GlobalVar globalVar;
    private API_Manager api_manager;
    private int recipe_id = -1;

    // Voice message
    private boolean isVoice_comment = false;
    private MediaRecorder mRecorder;
    private MediaPlayer mPlayer;
    private Handler mHandler = new Handler();
    private boolean isPlaying = false;
    private boolean isRecording = false;
    private String fileName = null;
    private int lastProgress = 0;
    private SeekBar seekBar;
    private Chronometer chronometer;
    private ImageView imageViewRecord, imageViewPlay;

    // Shake
    private ShakeListener mShaker;
    private boolean isShakeAlert = false;

    final String postcomment_key = "postcomment_api";
    final String postaudio_key = "postaudio_api";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);
        mActivity = this;
        bindViews();
        setListeners();

        globalVar = (GlobalVar) getApplicationContext();
        api_manager = new API_Manager(mActivity, mActivity, globalVar);
        Bundle extras = getIntent().getExtras();
        recipe_id = extras.getInt("recipe_id");

        Animation anim = AnimationUtils.loadAnimation(this, R.anim.frombottom);
        anim.setInterpolator((new AccelerateDecelerateInterpolator()));
        anim.setFillAfter(true);
        ll_mainpage_comment.setAnimation(anim);

        mShaker = new ShakeListener(this);
        mShaker.setOnShakeListener(new ShakeListener.OnShakeListener() {
            public void onShake() {
                if (!isShakeAlert) {
                    isShakeAlert = true;
                    new AlertDialog.Builder(mActivity)
                            .setTitle(mActivity.getString(R.string.clear_input))
                            .setMessage(mActivity.getString(R.string.confirm_clear_input))
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    clearAll();
                                    isShakeAlert = false;
                                }
                            })
                            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // do nothing
                                    isShakeAlert = false;
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mShaker.pause();
    }

    @Override
    public void receiveData(String key, String JSON) {
        switch (key) {
            case postcomment_key:
                PostcommentResult(JSON);
                break;
            case postaudio_key:
                PostcommentResult(JSON);
                break;

        }
    }

    private void bindViews() {
        edt_comment_chi = findViewById(R.id.edt_comment_chi);
        edt_comment = findViewById(R.id.edt_comment);
        tv_comment_chi = findViewById(R.id.tv_comment_chi);
        tv_comment = findViewById(R.id.tv_comment);
        btn_comment_confirm = findViewById(R.id.btn_comment_confirm);
        btn_comment_back = findViewById(R.id.btn_comment_back);
        ll_mainpage_comment = findViewById(R.id.ll_mainpage_comment);
        ll_voice_comment= findViewById(R.id.ll_voice_comment);
        swh_voice_comment= findViewById(R.id.swh_voice_comment);
        chronometer = (Chronometer) findViewById(R.id.chronometerTimer);
        chronometer.setBase(SystemClock.elapsedRealtime());

        seekBar = (SeekBar) findViewById(R.id.seekBar);
        imageViewRecord = (ImageView) findViewById(R.id.imageViewRecord);
        imageViewPlay = (ImageView) findViewById(R.id.imageViewPlay);
        imageViewPlay.setEnabled(false);
    }

    private void setListeners() {
        btn_comment_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (recipe_id >-1){
                    if (!isVoice_comment){
                        globalVar.showloadingDialog(mActivity,getResources().getString(R.string.newcomment));
                        PostComment();
                    }else{
                        globalVar.showloadingDialog(mActivity,getResources().getString(R.string.newcomment));
                        PostVoiceComment(recipe_id);
                    }

                }
            }
        });

        btn_comment_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        imageViewRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    recordButton();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });


        imageViewPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isPlaying){
                    lastProgress = 0;
                    chronometer.setBase(SystemClock.elapsedRealtime());
                    seekBar.setProgress(0);
                    startPlaying();
                }else{
                    stopPlaying();
                }

            }
        });

        swh_voice_comment.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    isVoice_comment = true;
                    showVoice(true);
                } else {
                    isVoice_comment = false;
                    showVoice(false);
                }
            }
        });

    }


    private void clearAll() {
        edt_comment_chi.setText("");
        edt_comment.setText("");
        fileName="";
        lastProgress = 0;
        seekBar.setProgress(0);
        imageViewPlay.setEnabled(false);
        imageViewPlay.setVisibility(View.INVISIBLE);
        seekBar.setVisibility(View.INVISIBLE);
    }

    public void showVoice(boolean isVoice_comment) {
        if (!isVoice_comment) {
            edt_comment_chi.setVisibility(View.VISIBLE);
            edt_comment.setVisibility(View.VISIBLE);
            tv_comment_chi.setVisibility(View.VISIBLE);
            tv_comment.setVisibility(View.VISIBLE);
            ll_voice_comment.setVisibility(View.GONE);
        } else {
            edt_comment_chi.setVisibility(View.GONE);
            edt_comment.setVisibility(View.GONE);
            tv_comment_chi.setVisibility(View.GONE);
            tv_comment.setVisibility(View.GONE);
            ll_voice_comment.setVisibility(View.VISIBLE);
        }

    }


    // Voice

    private void recordButton(){
        try{
            if (!isRecording){
                isRecording = true;
                startRecording();
            }else{
                isRecording = false;
                stopRecording();
                imageViewPlay.setVisibility(View.VISIBLE);
                seekBar.setVisibility(View.VISIBLE);
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void startRecording() {
        mRecorder = new MediaRecorder();
        mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        File root = android.os.Environment.getExternalStorageDirectory();
        File file = new File(root.getAbsolutePath() + "/VoiceRecorderSimplifiedCoding/Audios");
        if (!file.exists()) {
            file.mkdirs();
        }

        fileName =  root.getAbsolutePath() + "/VoiceRecorderSimplifiedCoding/Audios/" + String.valueOf(System.currentTimeMillis() + ".mp3");
        Log.d("filename",fileName);
        mRecorder.setOutputFile(fileName);
        mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

        try {
            mRecorder.prepare();
            mRecorder.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
        lastProgress = 0;
        seekBar.setProgress(0);
        stopPlaying();
        // making the imageview a stop button
        imageViewRecord.setImageResource(R.mipmap.ic_stop);
        //starting the chronometer
        chronometer.setBase(SystemClock.elapsedRealtime());
        chronometer.start();
    }

    private void stopPlaying() {
        try{
            mPlayer.release();
        }catch (Exception e){
            e.printStackTrace();
        }
        mPlayer = null;
        //showing the play button
        isPlaying = false;
        imageViewPlay.setImageResource(R.mipmap.ic_play);
        imageViewPlay.setEnabled(true);
        chronometer.stop();

    }

    private void stopRecording() {

        try{
            mRecorder.stop();
            mRecorder.release();
        }catch (Exception e){
            e.printStackTrace();
        }
        mRecorder = null;
        //starting the chronometer
        chronometer.stop();
        chronometer.setBase(SystemClock.elapsedRealtime());
        //showing the play button
        imageViewRecord.setImageResource(R.mipmap.ic_microphone);
        Toast.makeText(this, "Recording saved successfully.", Toast.LENGTH_SHORT).show();
    }

    private void startPlaying() {
        mPlayer = new MediaPlayer();
        try {
            mPlayer.setDataSource(fileName);
            mPlayer.prepare();
            mPlayer.start();
        } catch (IOException e) {
            Log.e("LOG_TAG", "prepare() failed");
        }
        //making the imageview pause button
        isPlaying = true;
        imageViewPlay.setImageResource(R.mipmap.ic_stop);

        seekBar.setProgress(lastProgress);
        mPlayer.seekTo(lastProgress);
        seekBar.setMax(mPlayer.getDuration());
        seekUpdation();
        chronometer.start();


        mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                chronometer.stop();
                isPlaying = false;
                imageViewPlay.setImageResource(R.mipmap.ic_play);
            }
        });



        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if( mPlayer!=null && fromUser ){
                    mPlayer.seekTo(progress);
                    chronometer.setBase(SystemClock.elapsedRealtime() - mPlayer.getCurrentPosition());
                    lastProgress = progress;
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            seekUpdation();
        }
    };

    private void seekUpdation() {
        if(mPlayer != null){
            int mCurrentPosition = mPlayer.getCurrentPosition() ;
            seekBar.setProgress(mCurrentPosition);
            lastProgress = mCurrentPosition;
        }
        mHandler.postDelayed(runnable, 100);
    }

    public void PostComment(){
        if (edt_comment.getText().toString().length()>0){
            JSON_CLASS.PostComment postItems = new JSON_CLASS.PostComment();
            postItems.recipeId = recipe_id;
            postItems.comment_chi = edt_comment_chi.getText().toString();
            postItems.comment_eng = edt_comment.getText().toString();

            final String inputJson = new Gson().toJson(postItems);
            final String url = "http://" + globalVar.domain + "/api/comment/createComment?token=" + globalVar.accessToken;

            api_manager.postAPI(postcomment_key, url, inputJson);
        }else{
            globalVar.toast(mActivity,getResources().getString(R.string.comment_empty));
            globalVar.dismissloadingDialog(mActivity);
        }
    }

    public void PostVoiceComment(int uploadedRecipeId){
        if (fileName!= null && fileName.length()>0){
            File file = new File(fileName);
            if (file !=null){
                String url = "http://" + globalVar.domain  + "/api/comment/createVoiceComment?token=" + globalVar.accessToken+"&&recipeId="+uploadedRecipeId;

                api_manager.postVoice(postaudio_key,url,file);
            }
        }else{
            globalVar.toast(mActivity,getResources().getString(R.string.voice_empty));
            globalVar.dismissloadingDialog(mActivity);
        }
    }


    private void PostcommentResult(String result) {
        JSON_CLASS.post_res obj_post_Result = null;
        obj_post_Result = new Gson().fromJson(result, new TypeToken<JSON_CLASS.post_res>() {
        }.getType());
        if (obj_post_Result != null) {
            if (obj_post_Result.Result) {
                globalVar.dismissloadingDialog(mActivity);
                finish();
            } else {
                if (obj_post_Result.message.equals("Auth failed")) {
                    globalVar.toast(mActivity, mActivity.getResources().getString(R.string.login_expired));
                } else {
                    globalVar.toast(mActivity, obj_post_Result.message);
                }
            }
        }
    }

}
