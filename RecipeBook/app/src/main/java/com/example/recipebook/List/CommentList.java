package com.example.recipebook.List;

public class CommentList {
    private int id;
    private int user_id;
    private int recipe_id;
    private int isVoice;
    private String voice_path;
    private String author;
    private String comment_eng;
    private String comment_chi;
    private String created_date;
    private String modified_date;

    public CommentList(int id, int user_id, int recipe_id, int isVoice, String voice_path, String author, String comment_eng, String comment_chi, String created_date, String modified_date) {
        this.id = id;
        this.user_id = user_id;
        this.recipe_id = recipe_id;
        this.isVoice = isVoice;
        this.voice_path = voice_path;
        this.author = author;
        this.comment_eng = comment_eng;
        this.comment_chi = comment_chi;
        this.created_date = created_date;
        this.modified_date = modified_date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getRecipe_id() {
        return recipe_id;
    }

    public void setRecipe_id(int recipe_id) {
        this.recipe_id = recipe_id;
    }

    public int getIsVoice() {
        return isVoice;
    }

    public void setIsVoice(int isVoice) {
        this.isVoice = isVoice;
    }

    public String getVoice_path() {
        return voice_path;
    }

    public void setVoice_path(String voice_path) {
        this.voice_path = voice_path;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getComment_eng() {
        return comment_eng;
    }

    public void setComment_eng(String comment_eng) {
        this.comment_eng = comment_eng;
    }

    public String getComment_chi() {
        return comment_chi;
    }

    public void setComment_chi(String comment_chi) {
        this.comment_chi = comment_chi;
    }

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }

    public String getModified_date() {
        return modified_date;
    }

    public void setModified_date(String modified_date) {
        this.modified_date = modified_date;
    }
}
