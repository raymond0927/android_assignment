package com.example.recipebook.Activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.StrictMode;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.BounceInterpolator;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.recipebook.API.APIInterface;
import com.example.recipebook.API.API_Manager;
import com.example.recipebook.Biometric.BiometricPromptManager;
import com.example.recipebook.GlobalVar;
import com.example.recipebook.API.JSON_CLASS;
import com.example.recipebook.R;
import com.example.recipebook.SharedPreferenceManager;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.Locale;

public class LoginActivity extends AppCompatActivity implements APIInterface {

    private final String LOG_TAG = this.getClass().getName();
    public GlobalVar globalVar;
    SharedPreferenceManager mSharedPreferenceManager;
    private API_Manager api_manager;
    LoginActivity mActivity;

    private final static int ALL_PERMISSIONS_RESULT = 101;
    LinearLayout ll_login;
    Button btn_register;
    Button btn_Login , btn_touchID;
    ImageView iv_login_setting;
    EditText edt_UserName, edt_Password;

    private BiometricPromptManager mManager;

    final String loginAPI_key = "login_api";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        SharedPreferenceManager.init(this);
        mSharedPreferenceManager = SharedPreferenceManager.getInstance();
        Log.v(LOG_TAG, mSharedPreferenceManager.getLanguage().toString());
        setLanguageForApp(mSharedPreferenceManager.getLanguage().toString());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        bindViews();
        setListeners();
        requestAppPermissions();
        Animation anim = AnimationUtils.loadAnimation(this, R.anim.fromtop);
        anim.setInterpolator((new BounceInterpolator()));
        anim.setFillAfter(true);
        ll_login.setAnimation(anim);
        mManager = BiometricPromptManager.from(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferenceManager.init(this);
        mSharedPreferenceManager = SharedPreferenceManager.getInstance();
        Log.v(LOG_TAG, mSharedPreferenceManager.getLanguage().toString());
        globalVar= (GlobalVar) getApplicationContext();
        globalVar.language =mSharedPreferenceManager.getLanguage().toString();
        mActivity = this;
        api_manager = new API_Manager(mActivity, mActivity, globalVar);
        if (mSharedPreferenceManager.getUsername()!=null && mSharedPreferenceManager.getUsername().length()>0){
            edt_UserName.setText(mSharedPreferenceManager.getUsername());
        }
    }

    @Override
    public void receiveData(String key, String JSON) {
        switch (key) {
            case loginAPI_key:
                loginResult(JSON);
                break;

        }
    }

    private void bindViews() {
        ll_login = findViewById(R.id.ll_login);
        btn_register = findViewById(R.id.btn_register);
        btn_Login = findViewById(R.id.btn_Login);
        btn_touchID = findViewById(R.id.btn_touchID);
        iv_login_setting = findViewById(R.id.iv_login_setting);
        edt_UserName = findViewById(R.id.edt_UserName);
        edt_Password = findViewById(R.id.edt_Password);
    }

    private void setListeners() {
        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });

        btn_Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (inputChecking()) {
                    globalVar.showloadingDialog(mActivity, mActivity.getResources().getString(R.string.login));
                    JSON_CLASS.PostLogin postItems = new JSON_CLASS.PostLogin();
                    postItems.password = edt_Password.getText().toString();
                    postItems.username = edt_UserName.getText().toString();

                    final String inputJson = new Gson().toJson(postItems);
                    final String url = "http://" + mSharedPreferenceManager.getDomain().toString() + "/api/users/login";

                    api_manager.postAPI(loginAPI_key, url, inputJson);
                }
            }
        });

        iv_login_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, SettingActivity.class);
                startActivity(intent);
            }
        });


        btn_touchID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mSharedPreferenceManager.getPassword() != null && mSharedPreferenceManager.getPassword().length() > 0) {
                    if (mManager.isBiometricPromptEnable()) {
                        mManager.authenticate(new BiometricPromptManager.OnBiometricIdentifyCallback() {
                            @Override
                            public void onUsePassword() {
                                globalVar.toast(mActivity, "onUsePassword");
                            }

                            @Override
                            public void onSucceeded() {
                                globalVar.showloadingDialog(mActivity, mActivity.getResources().getString(R.string.login));
                                JSON_CLASS.PostLogin postItems = new JSON_CLASS.PostLogin();
                                postItems.password = mSharedPreferenceManager.getPassword();
                                postItems.username = mSharedPreferenceManager.getUsername();

                                final String inputJson = new Gson().toJson(postItems);
                                final String url = "http://" + mSharedPreferenceManager.getDomain().toString() + "/api/users/login";

                                api_manager.postAPI(loginAPI_key, url, inputJson);
                            }

                            @Override
                            public void onFailed() {
                                globalVar.toast(mActivity, "onFailed");
                            }

                            @Override
                            public void onError(int code, String reason) {
                                globalVar.toast(mActivity, "onError");
                            }

                            @Override
                            public void onCancel() {

//                            Toast.makeText(mActivity, "onCancel", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                }else{
                    globalVar.toast(mActivity, mActivity.getResources().getString(R.string.touchid_error));
                }
            }

        });
    }

    public void loginResult(String loginResult) {
        JSON_CLASS.Login_res obj_loginResult = null;
        obj_loginResult = new Gson().fromJson(loginResult, new TypeToken<JSON_CLASS.Login_res>() {
        }.getType());
        if (obj_loginResult != null) {
            if (obj_loginResult.Result) {
                    mSharedPreferenceManager.setUsername(edt_UserName.getText().toString());
                    mSharedPreferenceManager.saveSharedPreference();
                globalVar.accessToken = obj_loginResult.token;
                globalVar.userId = obj_loginResult.userId;
                globalVar.userName = edt_UserName.getText().toString();
                if (edt_Password.getText().toString().length()>0){
                    globalVar.password = edt_Password.getText().toString();
                }else{
                    globalVar.password = mSharedPreferenceManager.getPassword();
                }

                globalVar.dismissloadingDialog(mActivity);
                globalVar.domain = mSharedPreferenceManager.getDomain();
                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            } else {
                globalVar.toast(mActivity, obj_loginResult.message);
                globalVar.dismissloadingDialog(mActivity);
            }
        }

    }


    public Boolean inputChecking() {
        if (edt_UserName.getText().toString().length() == 0) {
            globalVar.toast(mActivity, mActivity.getResources().getString(R.string.username_empty));
            return false;
        }

        if (edt_Password.getText().toString().length() == 0) {
            globalVar.toast(mActivity, mActivity.getResources().getString(R.string.password_empty));
            return false;
        }

        return true;

    }

    //
    // Permissions
    //

    private void requestAppPermissions() {
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            return;
        }

        if (hasReadPermissions() && hasWritePermissions() && hasCameraPermissions() && hasAidioPermissions()) {
            return;
        }

        ActivityCompat.requestPermissions(this,
                new String[]{
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA,
                        Manifest.permission.RECORD_AUDIO
                }, ALL_PERMISSIONS_RESULT); // your request code
    }

    private boolean hasReadPermissions() {
        return (ContextCompat.checkSelfPermission(getBaseContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
    }

    private boolean hasWritePermissions() {
        return (ContextCompat.checkSelfPermission(getBaseContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
    }

    private boolean hasCameraPermissions() {
        return (ContextCompat.checkSelfPermission(getBaseContext(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED);
    }

    private boolean hasAidioPermissions() {
        return (ContextCompat.checkSelfPermission(getBaseContext(), Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED);
    }

    private void setLanguageForApp(String languageToLoad) {
        Locale locale;
        if (languageToLoad.equals("not-set")) { //use any value for default
            locale = Locale.getDefault();
        } else {
            locale = new Locale(languageToLoad);
        }
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());
    }
}
