package com.example.recipebook;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by user on 26/4/2018.
 */

public class SharedPreferenceManager {
    private static final String TAG = SharedPreferenceManager.class.getSimpleName();

    private String domain;
    private Boolean isRemember ;
    private String username;
    private String password;
    private String language;


    private final SharedPreferences sharedPreferenceSettings;
    private static SharedPreferenceManager INSTANCE;

    public static SharedPreferenceManager getInstance(){
        return INSTANCE;
    }
    public static void init(Context context){
        if(INSTANCE == null){
            INSTANCE = new SharedPreferenceManager(context);
        }
    }
    private SharedPreferenceManager(Context context) {
        sharedPreferenceSettings = PreferenceManager.getDefaultSharedPreferences(context);
        updateSharedPreference();
    }


    public final void updateSharedPreference() {
        domain = sharedPreferenceSettings.getString(SharePreferenceConstant.SHARE_KEY_DOMAIN, "immense-castle-74565.herokuapp.com");
        isRemember = sharedPreferenceSettings.getBoolean(SharePreferenceConstant.SHARE_KEY_REMEMBER, false);
        username = sharedPreferenceSettings.getString(SharePreferenceConstant.SHARE_KEY_USERNAME, "");
        password = sharedPreferenceSettings.getString(SharePreferenceConstant.SHARE_KEY_PASSWORD, "");
        language = sharedPreferenceSettings.getString(SharePreferenceConstant.SHARE_KEY_LAN, "en");
    }

    public final void saveSharedPreference() {
        final SharedPreferences.Editor editor = sharedPreferenceSettings.edit();
        editor.putString(SharePreferenceConstant.SHARE_KEY_DOMAIN, domain);
        editor.putBoolean(SharePreferenceConstant.SHARE_KEY_REMEMBER, isRemember);
        editor.putString(SharePreferenceConstant.SHARE_KEY_USERNAME, username);
        editor.putString(SharePreferenceConstant.SHARE_KEY_PASSWORD, password);
        editor.putString(SharePreferenceConstant.SHARE_KEY_LAN, language);

        editor.apply();
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public Boolean getRemember() {
        return isRemember;
    }

    public void setRemember(Boolean remember) {
        isRemember = remember;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
}
