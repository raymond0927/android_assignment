package com.example.recipebook.Activity;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.recipebook.API.APIInterface;
import com.example.recipebook.API.API_Manager;
import com.example.recipebook.GlobalVar;
import com.example.recipebook.API.JSON_CLASS;
import com.example.recipebook.List.EditImageList;
import com.example.recipebook.R;
import com.example.recipebook.ShakeListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class RecipeEditActivity extends AppCompatActivity  implements APIInterface {

    private final String LOG_TAG = this.getClass().getName();
    private GlobalVar globalVar;
    private API_Manager api_manager;

    private static final int CAMERA_TAKE_REQUEST = 200;
    private static final int GALLERY_SELECT_REQUEST = 100;
    List<EditImageList> fileList = new ArrayList<>();
    List<Integer> deletedIDList = new ArrayList<>();
    List<JSON_CLASS.ImageInfo> imageInfoList = new ArrayList<>();
    private int recipeid;


    boolean isEdit = false;
    private RecipeEditActivity mActivity;
    private LinearLayout rl_mainpage_recipeEdit;
    private EditText edt_recipe_name, edt_recipe_ingredients, edt_recipe_methods, edt_recipe_name_chi, edt_recipe_ingredients_chi, edt_recipe_methods_chi;
    private ImageView iv_edit_image1, iv_edit_image2, iv_edit_image3, btn_edit_back;
    private TextView tv_recipe_name_chi, tv_recipe_ingredients_chi, tv_recipe_methods_chi;
    private Switch swh_Chinese_recipe;
    private Button btn_edit_confirm;

    private ShakeListener mShaker;
    private boolean isShakeAlert = false;
    boolean isChinese = false;
    File file_camera;
    Uri uri_camera;
    int pressedImageID = -1;

    final String postImageInfo_key = "postImageInfo";
    final String postRecipe_key = "postRecipe";
    final String delImage_key = "delImageInfo";
    final String putRecipe_key = "putRecipe";
    String recipe_info_json="";
    String recipe_image_json="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe_edit);
        globalVar = (GlobalVar) getApplicationContext();
        mActivity = this;
        api_manager = new API_Manager(mActivity, mActivity, globalVar);
        Bundle extras = getIntent().getExtras();
        isEdit = extras.getBoolean("isEdit");

        bindViews();
        setListeners();
        Animation anim = AnimationUtils.loadAnimation(this, R.anim.frombottom);
        anim.setInterpolator((new AccelerateDecelerateInterpolator()));
        anim.setFillAfter(true);
        rl_mainpage_recipeEdit.setAnimation(anim);
        fileList.add(new EditImageList(false, null));
        fileList.add(new EditImageList(false, null));
        fileList.add(new EditImageList(false, null));
        if (isEdit){
            recipe_info_json = extras.getString("recipe_info_json");
            recipe_image_json = extras.getString("recipe_image_json");
            if (recipe_info_json.length()>0){
                getRecipeByid(recipe_info_json);
            }
            if (recipe_image_json.length()>0){
                getImageByRecipeid(recipe_image_json);
            }
        }


        mShaker = new ShakeListener(this);
        mShaker.setOnShakeListener(new ShakeListener.OnShakeListener() {
            public void onShake() {
                if (!isShakeAlert) {
                    isShakeAlert = true;
                    if (!isEdit){
                        new AlertDialog.Builder(mActivity)
                                .setTitle(mActivity.getString(R.string.clear_input))
                                .setMessage(mActivity.getString(R.string.confirm_clear_input))
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        clearAll();
                                        isShakeAlert = false;
                                    }
                                })
                                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // do nothing
                                        isShakeAlert = false;
                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();
                    }else{
                        new AlertDialog.Builder(mActivity)
                                .setTitle(mActivity.getString(R.string.revert_input))
                                .setMessage(mActivity.getString(R.string.confirm_revert_input))
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        getRecipeByid(recipe_info_json);
                                        getImageByRecipeid(recipe_image_json);
                                        isShakeAlert = false;
                                    }
                                })
                                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // do nothing
                                        isShakeAlert = false;
                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();
                    }

                }
            }
        });
    }

    @Override
    public void onResume() {
        mShaker.resume();
        super.onResume();
    }

    @Override
    public void onPause() {
        mShaker.pause();
        super.onPause();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case CAMERA_TAKE_REQUEST:

                if (file_camera.exists()) {
                    Uri imageUri = Uri.fromFile(file_camera);
                    switch (pressedImageID) {
                        case 0:
                            addImage(0, file_camera);
                            Glide.with(mActivity)
                                    .load(imageUri)
                                    .into(iv_edit_image1);
                            break;
                        case 1:
                            addImage(1, file_camera);
                            Glide.with(mActivity)
                                    .load(imageUri)
                                    .into(iv_edit_image2);
                            break;
                        case 2:
                            addImage(2, file_camera);
                            Glide.with(mActivity)
                                    .load(imageUri)
                                    .into(iv_edit_image3);
                            break;
                    }
                }

                break;
            case GALLERY_SELECT_REQUEST:
                if (resultCode == RESULT_OK && data != null) {
                    String path = getPath(data.getData());
                    File file = new File(path);
                    Uri imageUri = Uri.fromFile(file);
                    switch (pressedImageID) {
                        case 0:
                            addImage(0, file);
                            Glide.with(mActivity)
                                    .load(imageUri)
                                    .into(iv_edit_image1);
                            break;
                        case 1:
                            addImage(1, file);
                            Glide.with(mActivity)
                                    .load(imageUri)
                                    .into(iv_edit_image2);
                            break;
                        case 2:
                            addImage(2, file);
                            Glide.with(mActivity)
                                    .load(imageUri)
                                    .into(iv_edit_image3);
                            break;
                    }
                }
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try{
            globalVar.dismissloadingDialog(mActivity);
            mShaker.pause();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void receiveData(String key, String JSON) {
        switch (key) {
            case postImageInfo_key:
                globalVar.dismissloadingDialog(mActivity);
                postImageInfoResult(JSON);
                finish();
                break;
            case postRecipe_key:
                postRecipeResult(JSON);
                break;
            case putRecipe_key:
                putRecipeResult(JSON);
                break;
            case delImage_key:
                delImageResult(JSON);
                break;
        }
    }

    private void bindViews() {
        rl_mainpage_recipeEdit = findViewById(R.id.rl_mainpage_recipeEdit);
        edt_recipe_name = findViewById(R.id.edt_recipe_name);
        edt_recipe_ingredients = findViewById(R.id.edt_recipe_ingredients);
        edt_recipe_methods = findViewById(R.id.edt_recipe_methods);
        iv_edit_image1 = findViewById(R.id.iv_edit_image1);
        iv_edit_image2 = findViewById(R.id.iv_edit_image2);
        iv_edit_image3 = findViewById(R.id.iv_edit_image3);
        btn_edit_confirm = findViewById(R.id.btn_edit_confirm);
        btn_edit_back = findViewById(R.id.btn_edit_back);
        edt_recipe_name_chi = findViewById(R.id.edt_recipe_name_chi);
        edt_recipe_ingredients_chi = findViewById(R.id.edt_recipe_ingredients_chi);
        edt_recipe_methods_chi = findViewById(R.id.edt_recipe_methods_chi);
        tv_recipe_name_chi = findViewById(R.id.tv_recipe_name_chi);
        tv_recipe_ingredients_chi = findViewById(R.id.tv_recipe_ingredients_chi);
        tv_recipe_methods_chi = findViewById(R.id.tv_recipe_methods_chi);
        swh_Chinese_recipe = findViewById(R.id.swh_Chinese_recipe);

    }

    private void setListeners() {
        iv_edit_image1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pressedImageID = 0;
                if (!fileList.get(0).isExist()) {
                    showPictureDialog();
                } else {
                    if (fileList.get(0).getFile()== null){
                        if (imageInfoList.size()>pressedImageID){
                            deletedIDList.add(imageInfoList.get(pressedImageID).id);
                        }
                    }
                    removeImage(pressedImageID);
                    iv_edit_image1.setImageResource(R.mipmap.ic_plus);
                }
            }
        });
        iv_edit_image2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pressedImageID = 1;
                if (!fileList.get(1).isExist()) {
                    showPictureDialog();
                } else {
                    if (fileList.get(0).getFile()== null){
                        if (imageInfoList.size()>pressedImageID){
                            deletedIDList.add(imageInfoList.get(pressedImageID).id);
                        }
                    }
                    removeImage(pressedImageID);
                    iv_edit_image2.setImageResource(R.mipmap.ic_plus);
                }
            }
        });

        iv_edit_image3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pressedImageID = 2;
                if (!fileList.get(2).isExist()) {
                    showPictureDialog();
                } else {
                    if (fileList.get(0).getFile()== null){
                        if (imageInfoList.size()>pressedImageID){
                            deletedIDList.add(imageInfoList.get(pressedImageID).id);
                        }
                    }
                    removeImage(pressedImageID);
                    iv_edit_image3.setImageResource(R.mipmap.ic_plus);
                }
            }
        });

        btn_edit_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (inputChecking()) {
                    if (!isEdit){
                        globalVar.showloadingDialog(mActivity, mActivity.getResources().getString(R.string.create_recipe));
                        postRecipe();
                    }else{
                        globalVar.showloadingDialog(mActivity, mActivity.getResources().getString(R.string.edit_recipe));
                        editChecking();
                    }
                }
            }
        });

        btn_edit_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        swh_Chinese_recipe.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    isChinese = true;
                    showChinese(true);
                } else {
                    isChinese = false;
                    showChinese(false);
                }
            }
        });

        edt_recipe_methods_chi.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View view, MotionEvent event) {

                if (view.getId() == R.id.edt_recipe_methods_chi && edt_recipe_methods_chi.getLineCount()>5) {
                    view.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            view.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });

        edt_recipe_methods.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View view, MotionEvent event) {

                if (view.getId() == R.id.edt_recipe_methods && edt_recipe_methods.getLineCount()>5) {
                    view.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            view.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });

        edt_recipe_ingredients.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View view, MotionEvent event) {

                if (view.getId() == R.id.edt_recipe_ingredients && edt_recipe_ingredients.getLineCount()>5) {
                    view.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            view.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });

        edt_recipe_ingredients_chi.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View view, MotionEvent event) {

                if (view.getId() == R.id.edt_recipe_ingredients_chi && edt_recipe_ingredients_chi.getLineCount()>5) {
                    view.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            view.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });
    }

    public void showChinese(boolean isChinese) {
        if (isChinese) {
            edt_recipe_name_chi.setVisibility(View.VISIBLE);
            edt_recipe_ingredients_chi.setVisibility(View.VISIBLE);
            edt_recipe_methods_chi.setVisibility(View.VISIBLE);
            tv_recipe_name_chi.setVisibility(View.VISIBLE);
            tv_recipe_ingredients_chi.setVisibility(View.VISIBLE);
            tv_recipe_methods_chi.setVisibility(View.VISIBLE);
        } else {
            edt_recipe_name_chi.setVisibility(View.GONE);
            edt_recipe_ingredients_chi.setVisibility(View.GONE);
            edt_recipe_methods_chi.setVisibility(View.GONE);
            tv_recipe_name_chi.setVisibility(View.GONE);
            tv_recipe_ingredients_chi.setVisibility(View.GONE);
            tv_recipe_methods_chi.setVisibility(View.GONE);
        }

    }

    private void clearAll() {
        edt_recipe_name.setText("");
        edt_recipe_ingredients.setText("");
        edt_recipe_methods.setText("");
        iv_edit_image1.setImageResource(R.mipmap.ic_plus);
        iv_edit_image2.setImageResource(R.mipmap.ic_plus);
        iv_edit_image3.setImageResource(R.mipmap.ic_plus);
        edt_recipe_name_chi.setText("");
        edt_recipe_ingredients_chi.setText("");
        edt_recipe_methods_chi.setText("");
        for(int i=0;i<fileList.size();i++){
            removeImage(i);
        }
    }

    public Boolean inputChecking (){
        if (edt_recipe_name.getText().toString().length() == 0) {
            globalVar.toast(mActivity, mActivity.getResources().getString(R.string.title_empty));
            return false;
        }

        if (edt_recipe_ingredients.getText().toString().length() == 0) {
            globalVar.toast(mActivity, mActivity.getResources().getString(R.string.ingredients_empty));
            return false;
        }

        if (edt_recipe_methods.getText().toString().length() == 0) {
            globalVar.toast(mActivity, mActivity.getResources().getString(R.string.methods_empty));
            return false;
        }

        int count = 0 ;
        for (int i = 0; i < fileList.size(); i++) {
            if (fileList.get(i).isExist()) {
                count++;
            }
        }
        Log.v("insert ",count+"");
        if (count == 0 ){
            globalVar.toast(mActivity, mActivity.getResources().getString(R.string.image_empty));
            return false;
        }

        return true;
    }

    public void editChecking(){
        if (inputChecking()){
            for (int deletedid : deletedIDList){
                Log.v("deletedid ",deletedid+"");
            }
            if (deletedIDList.size()>0){
                delImage(deletedIDList);
            }else{
                putRecipe();
            }
        }
    }




    //show image


    private void showPictureDialog() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(this);
        pictureDialog.setTitle("Select Action");
        String[] pictureDialogItems = {
                "Select photo from Gallery",
                "Capture photo from Camera"};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                Log.v("test", "gallery");
                                Intent fintent = new Intent(Intent.ACTION_GET_CONTENT);
                                fintent.setType("image/*");
                                try {
                                    startActivityForResult(fintent, GALLERY_SELECT_REQUEST);
                                } catch (ActivityNotFoundException e) {
                                    e.printStackTrace();
                                }
                                break;
                            case 1:
                                Log.v("test", "camera");
                                launchCamera();
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }


    // Camera

    public boolean checkCameraExists() {
        return mActivity.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA);
    }

    private void launchCamera() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);

        file_camera = new File(Environment.getExternalStorageDirectory(), String.valueOf(System.currentTimeMillis()) + ".jpg");
//        uri = FileProvider.getUriForFile(activity, activity.getApplicationContext().getPackageName() + ".provider", file);
        uri_camera = FileProvider.getUriForFile(this, "com.recipebook.fileprovider", file_camera);
        intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, uri_camera);

        startActivityForResult(intent, CAMERA_TAKE_REQUEST);
    }

    public String getPath(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        String document_id = cursor.getString(0);
        document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
        cursor.close();

        cursor = getContentResolver().query(
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
        cursor.moveToFirst();
        String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
        cursor.close();

        return path;
    }


    public void removeImage(int position) {
        fileList.get(position).setExist(false);
        fileList.get(position).setFile(null);
    }

    public void addImage(int position, File imageFile) {
        fileList.get(position).setExist(true);
        fileList.get(position).setFile(imageFile);
    }

    public void postRecipe(){
        JSON_CLASS.PostRecipe postItems = new JSON_CLASS.PostRecipe();
        postItems.ingredient_chi = edt_recipe_ingredients_chi.getText().toString();
        postItems.ingredient_eng = edt_recipe_ingredients.getText().toString();
        postItems.methods_chi = edt_recipe_methods_chi.getText().toString();
        postItems.methods_eng = edt_recipe_methods.getText().toString();
        postItems.title_chi = edt_recipe_name_chi.getText().toString();
        postItems.title_eng = edt_recipe_name.getText().toString();

        final String inputJson = new Gson().toJson(postItems);

        String url = "http://" +globalVar.domain + "/api/recipe/createRecipe?token=" + globalVar.accessToken;
        api_manager.postAPI(postRecipe_key,url,inputJson);
    }

    public void putRecipe(){
        JSON_CLASS.PutRecipe putItems = new JSON_CLASS.PutRecipe();
        putItems.recipeId = recipeid;
        putItems.ingredient_chi = edt_recipe_ingredients_chi.getText().toString();
        putItems.ingredient_eng = edt_recipe_ingredients.getText().toString();
        putItems.methods_chi = edt_recipe_methods_chi.getText().toString();
        putItems.methods_eng = edt_recipe_methods.getText().toString();
        putItems.title_chi = edt_recipe_name_chi.getText().toString();
        putItems.title_eng = edt_recipe_name.getText().toString();

        final String inputJson = new Gson().toJson(putItems);

        String url = "http://" +globalVar.domain + "/api/recipe/editRecipe?token=" + globalVar.accessToken;
        api_manager.putAPI(putRecipe_key,url,inputJson);
    }

    public void delImage(List<Integer> deletedIDList){
        JSON_CLASS.DelImage delImageItems = new JSON_CLASS.DelImage();
        delImageItems.imageId = deletedIDList;

        final String inputJson = new Gson().toJson(delImageItems);

        String url = "http://" +globalVar.domain + "/api/recipe_image/deleteImage?token=" + globalVar.accessToken;
        api_manager.delAPI(delImage_key,url,inputJson);
    }


    private void postRecipeResult(String result) {
        JSON_CLASS.postRecipe_res obj_postRecipe_Result = null;
        obj_postRecipe_Result = new Gson().fromJson(result, new TypeToken<JSON_CLASS.postRecipe_res>() {
        }.getType());
        if (obj_postRecipe_Result != null) {
            if (obj_postRecipe_Result.Result) {
                postImageInfo( obj_postRecipe_Result.id);
            } else {
                JSON_CLASS.fail_res obj_fail = new Gson().fromJson(result, new TypeToken<JSON_CLASS.fail_res>() {
                }.getType());
                if (obj_fail.message.equals("Auth failed")) {
                    globalVar.toast(mActivity, mActivity.getResources().getString(R.string.login_expired));
                    Intent intent = new Intent(mActivity, LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                } else {
                    globalVar.toast(mActivity, obj_fail.message);
                }
                globalVar.dismissloadingDialog(mActivity);
            }
        }
    }

    private void putRecipeResult(String result) {
        JSON_CLASS.post_res obj_put_Result = null;
        obj_put_Result = new Gson().fromJson(result, new TypeToken<JSON_CLASS.post_res>() {
        }.getType());
        if (obj_put_Result != null) {
            if (obj_put_Result.Result) {
                int count = 0;
                for (int i =0;i<fileList.size();i++){
                    if (fileList.get(i).getFile()!=null){
                        count++;
                    }
                }
                if (count>0){
                    postImageInfo(recipeid);
                }else {
                    globalVar.dismissloadingDialog(mActivity);
                    finish();
                }

            } else {
                if (obj_put_Result.message.equals("Auth failed")) {
                    globalVar.toast(mActivity, mActivity.getResources().getString(R.string.login_expired));
                    Intent intent = new Intent(mActivity, LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                } else {
                    globalVar.toast(mActivity, obj_put_Result.message);
                }
                globalVar.dismissloadingDialog(mActivity);
            }
        }
    }

    private void delImageResult(String result) {
        JSON_CLASS.post_res obj_del_Result = null;
        obj_del_Result = new Gson().fromJson(result, new TypeToken<JSON_CLASS.post_res>() {
        }.getType());
        if (obj_del_Result != null) {
            if (obj_del_Result.Result) {
                putRecipe();
            } else {
                if (obj_del_Result.message.equals("Auth failed")) {
                    globalVar.toast(mActivity, mActivity.getResources().getString(R.string.login_expired));
                    Intent intent = new Intent(mActivity, LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                } else {
                    globalVar.toast(mActivity, obj_del_Result.message);
                }
                globalVar.dismissloadingDialog(mActivity);
            }
        }
    }

    public void postImageInfo(int uploadedRecipeId ){
        List<File> uploadList = new ArrayList<>();
        for (int i = 0; i < fileList.size(); i++) {
            if (fileList.get(i).isExist() && fileList.get(i).getFile()!=null) {
                Log.v("uploadList"+i,"upload1!!!!");
                uploadList.add(fileList.get(i).getFile());
            }
        }

        String url = "http://" + globalVar.domain  + "/api/recipe_image/createImage?token=" + globalVar.accessToken+"&&recipeId="+uploadedRecipeId;
        api_manager.postImage(postImageInfo_key,url,uploadList);
    }

    private void postImageInfoResult(String result) {
        JSON_CLASS.postImageInfo_res obj_postImageInfo_Result = null;
        obj_postImageInfo_Result = new Gson().fromJson(result, new TypeToken<JSON_CLASS.postImageInfo_res>() {
        }.getType());
        if (obj_postImageInfo_Result != null) {
            if (obj_postImageInfo_Result.Result) {
                globalVar.toast(mActivity,obj_postImageInfo_Result.message);
            } else {
                JSON_CLASS.fail_res obj_fail = new Gson().fromJson(result, new TypeToken<JSON_CLASS.fail_res>() {
                }.getType());
                if (obj_fail.message.equals("Auth failed")) {
                    globalVar.toast(mActivity, mActivity.getResources().getString(R.string.login_expired));
                    Intent intent = new Intent(mActivity, LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                } else {
                    globalVar.toast(mActivity, obj_fail.message);
                }
                globalVar.dismissloadingDialog(mActivity);
            }
        }
    }

    private void getRecipeByid(String result) {
        JSON_CLASS.getRecipeByid obj_recipe_Result = null;
        obj_recipe_Result = new Gson().fromJson(result, new TypeToken<JSON_CLASS.getRecipeByid>() {}.getType());
        if (obj_recipe_Result != null) {
            if (obj_recipe_Result.Result) {
                recipeid = obj_recipe_Result.message.get(0).id;
                final String ingredient_chi =  obj_recipe_Result.message.get(0).ingredient_chi;
                final String methods_chi =  obj_recipe_Result.message.get(0).methods_chi;
                final String title_chi =  obj_recipe_Result.message.get(0).title_chi;
                final String ingredient_eng =  obj_recipe_Result.message.get(0).ingredient_eng;
                final String methods_eng =  obj_recipe_Result.message.get(0).methods_eng;
                final String title_eng =  obj_recipe_Result.message.get(0).title_eng;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        swh_Chinese_recipe.setChecked(false);
                        if ((ingredient_chi!=null && ingredient_chi.length()>0) ||(methods_chi!=null && methods_chi.length()>0)||(title_chi!=null &&title_chi.length()>0)){
                            swh_Chinese_recipe.setChecked(true);
                        }
                        edt_recipe_name_chi.setText(title_chi);
                        edt_recipe_ingredients_chi.setText(ingredient_chi);
                        edt_recipe_methods_chi.setText(methods_chi);
                        edt_recipe_name.setText(title_eng);
                        edt_recipe_ingredients.setText(ingredient_eng);
                        edt_recipe_methods.setText(methods_eng);
                    }
                });

            }
        }
    }

    private void getImageByRecipeid(String result) {
        JSON_CLASS.getImageByRecipeid obj_image_Result = null;
        obj_image_Result = new Gson().fromJson(result, new TypeToken<JSON_CLASS.getImageByRecipeid>() {
        }.getType());
        if (obj_image_Result != null) {
            if (obj_image_Result.Result) {
                imageInfoList.clear();
                imageInfoList.addAll(obj_image_Result.message);
                for (int i =0; i<obj_image_Result.message.size();i++) {
                    if (i == 0) {
                        fileList.get(i).setExist(true);
                        Glide.with(mActivity)
                                .load(obj_image_Result.message.get(i).image_path)
                                .into(iv_edit_image1);
                    }
                    if (i == 1) {
                        fileList.get(i).setExist(true);
                        Glide.with(mActivity)
                                .load(obj_image_Result.message.get(i).image_path)
                                .into(iv_edit_image2);
                    }
                    if (i == 2) {
                        fileList.get(i).setExist(true);
                        Glide.with(mActivity)
                                .load(obj_image_Result.message.get(i).image_path)
                                .into(iv_edit_image3);
                    }
                }
            } else {
                JSON_CLASS.fail_res obj_fail = new Gson().fromJson(result, new TypeToken<JSON_CLASS.fail_res>() {
                }.getType());
                if (obj_fail.message.equals("Auth failed")) {
                    mActivity.globalVar.toast(mActivity, mActivity.getResources().getString(R.string.login_expired));
                } else {
                    mActivity.globalVar.toast(mActivity, obj_fail.message);
                }
            }
        }
    }

}
