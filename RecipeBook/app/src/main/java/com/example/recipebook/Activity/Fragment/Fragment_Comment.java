package com.example.recipebook.Activity.Fragment;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.recipebook.Activity.InfoActivity;
import com.example.recipebook.Activity.LoginActivity;
import com.example.recipebook.API.CallBackClass;
import com.example.recipebook.API.JSON_CLASS;
import com.example.recipebook.List.CommentList;
import com.example.recipebook.List.CommentListAdapter;
import com.example.recipebook.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

public class Fragment_Comment extends Fragment {

    private final String LOG_TAG = this.getClass().getName();
    private InfoActivity mActivity;
    List<CommentList> commentLists = new ArrayList<>();
    private CommentListAdapter adapter;
    private ListView lv_comment;

    public Handler aiHandler;

    public Fragment_Comment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fg_comment, container, false);
        mActivity = InfoActivity.getInstance();

        lv_comment = view.findViewById(R.id.lv_comment);

        adapter = new CommentListAdapter(mActivity, commentLists);
        lv_comment.setAdapter(adapter);


        aiHandler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message inputMessage) {
                try {
                    CallBackClass result = (CallBackClass) inputMessage.obj;
                    Log.e(LOG_TAG, "AiHandler handle message (" + result.object + "):" + result.result);
                    getCommentByid(result.result);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        };

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        try{
            if (adapter.timer != null){
                adapter.timer.cancel();
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        try{
            if (adapter.mPlayer != null){
                adapter.mPlayer.release();
                adapter.mPlayer = null;
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void getCommentByid(String result) {
        JSON_CLASS.getCommentByid obj_comment_Result = null;
        try{
            obj_comment_Result = new Gson().fromJson(result, new TypeToken<JSON_CLASS.getCommentByid>() {}.getType());
        }catch (Exception e){
            e.printStackTrace();
        }
        Log.v("chihi",(obj_comment_Result != null)+"");
        if (obj_comment_Result != null) {
            if (obj_comment_Result.Result) {
                Log.v("comments List123",obj_comment_Result.message.get(0).getAuthor());
                commentLists.clear();
                commentLists.addAll(obj_comment_Result.message);
                for (int i =0;i<commentLists.size();i++){
                    Log.v("comments List",commentLists.get(i).getAuthor());
                }

                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        adapter.notifyDataSetChanged();
                    }
                });

            }
        }else{
            JSON_CLASS.fail_res obj_fail = new Gson().fromJson(result, new TypeToken<JSON_CLASS.fail_res>() {
            }.getType());
            if (obj_fail.message.equals("Auth failed")) {
                mActivity.globalVar.toast(mActivity, mActivity.getResources().getString(R.string.login_expired));
                Intent intent = new Intent(mActivity, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            } else {
                mActivity.globalVar.toast(mActivity, obj_fail.message);
            }

        }
    }

}
