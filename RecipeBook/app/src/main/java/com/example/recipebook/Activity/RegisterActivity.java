package com.example.recipebook.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.BounceInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.recipebook.API.APIInterface;
import com.example.recipebook.API.API_Manager;
import com.example.recipebook.GlobalVar;
import com.example.recipebook.API.JSON_CLASS;
import com.example.recipebook.R;
import com.example.recipebook.SharedPreferenceManager;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class RegisterActivity extends AppCompatActivity  implements APIInterface {

    private final String LOG_TAG = this.getClass().getName();
    private GlobalVar globalVar;
    SharedPreferenceManager mSharedPreferenceManager;
    private API_Manager api_manager;
    RegisterActivity mActivity;
    Gson gson;

    private ImageView iv_register_back;
    private EditText edt_register_UserName , edt_register_email, edt_register_Password, edt_register_ConfirmPassword;
    private Button btn_register_confirm;

    final String register_key = "register_post";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        LinearLayout ll_register = findViewById(R.id.ll_register);

        gson = new Gson();
        bindViews();
        setListeners();

        Animation anim = AnimationUtils.loadAnimation(this, R.anim.fromtop);
        anim.setInterpolator((new BounceInterpolator()));
        anim.setFillAfter(true);
        ll_register.setAnimation(anim);


    }

    @Override
    protected void onResume() {
        super.onResume();
        globalVar = (GlobalVar) getApplicationContext();
        SharedPreferenceManager.init(this);
        mSharedPreferenceManager = SharedPreferenceManager.getInstance();
        Log.v(LOG_TAG, mSharedPreferenceManager.getDomain().toString());
        mActivity = this;
        api_manager = new API_Manager(mActivity,mActivity,globalVar);
    }

    private void bindViews() {
        iv_register_back = findViewById(R.id.iv_register_back);
        edt_register_UserName = findViewById(R.id.edt_register_UserName);
        edt_register_email = findViewById(R.id.edt_register_email);
        edt_register_Password = findViewById(R.id.edt_register_Password);
        edt_register_ConfirmPassword = findViewById(R.id.edt_register_ConfirmPassword);
        btn_register_confirm = findViewById(R.id.btn_register_confirm);

    }

    private void setListeners() {
        btn_register_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (inputChecking()){
                    globalVar.showloadingDialog(mActivity,mActivity.getResources().getString(R.string.register));
                    JSON_CLASS.PostRegister postItems = new JSON_CLASS.PostRegister();
                    postItems.password = edt_register_Password.getText().toString();
                    postItems.username = edt_register_UserName.getText().toString();
                    postItems.email = edt_register_email.getText().toString();

                    final String inputJson = gson.toJson(postItems);
                    final String url = "http://" + mSharedPreferenceManager.getDomain().toString() + "/api/users/sign";

                    api_manager.postAPI(register_key,url,inputJson);
                }
            }
        });


        iv_register_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public Boolean inputChecking(){
        if (edt_register_UserName.getText().toString().length()==0){
            globalVar.toast(mActivity,mActivity.getResources().getString(R.string.username_empty));
            return false;
        }

        if (edt_register_email.getText().toString().length()==0){
            globalVar.toast(mActivity,mActivity.getResources().getString(R.string.email_empty));
            return false;
        }

        if (edt_register_Password.getText().toString().length()==0){
            globalVar.toast(mActivity,mActivity.getResources().getString(R.string.password_empty));
            return false;
        }

        if (edt_register_ConfirmPassword.getText().toString().length()==0){
            globalVar.toast(mActivity,mActivity.getResources().getString(R.string.password_empty));
            return false;
        }

        if (!edt_register_ConfirmPassword.getText().toString().equals(edt_register_Password.getText().toString())){
            globalVar.toast(mActivity,mActivity.getResources().getString(R.string.password_not_match));
            return false;
        }

        return true;

    }


    @Override
    public void receiveData(String key, String JSON) {
        switch(key){
            case register_key:
                registerResult(JSON);
                break;

        }
    }

    private void registerResult(String result ){
        JSON_CLASS.Register_res obj_RegisterResult = null;
        obj_RegisterResult = gson.fromJson(result, new TypeToken<JSON_CLASS.Register_res>() {
        }.getType());
        if (obj_RegisterResult != null) {
            if (obj_RegisterResult.Result) {
                globalVar.toast(mActivity, obj_RegisterResult.message);
                finish();
            } else {
                globalVar.toast(mActivity, obj_RegisterResult.message);
                finish();
            }
        }
    }
}
