package com.example.recipebook.List;

import java.io.File;

public class EditImageList {

    private boolean isExist = false;
    private File file;

    public EditImageList(boolean isExist, File file) {
        this.isExist = isExist;
        this.file = file;
    }

    public boolean isExist() {
        return isExist;
    }

    public void setExist(boolean exist) {
        isExist = exist;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }
}
