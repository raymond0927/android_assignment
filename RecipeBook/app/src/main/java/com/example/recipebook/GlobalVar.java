package com.example.recipebook;

import android.app.Activity;
import android.app.Application;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.Toast;


import com.example.recipebook.Activity.LoginActivity;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


public class GlobalVar extends Application {

    private static Toast myToast;
    private static ProgressDialog dialog;
    public String accessToken = "";
    public String domain = "";
    public int userId = -1;
    public String userName = "";
    public String password = "";
    public String language = "";


    public static void toast(final Activity activity, final String text) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (myToast != null) {
                    myToast.cancel();
                    myToast = Toast.makeText(activity, text, Toast.LENGTH_SHORT);
                } else {
                    myToast = Toast.makeText(activity, text, Toast.LENGTH_SHORT);
                }
                myToast.show();
            }
        });
    }

    public static void showloadingDialog(final Activity activity, final String title) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try{
                        dialog = ProgressDialog.show(activity, title,
                                activity.getResources().getString(R.string.loading), true);
                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        });
    }

    public static void dismissloadingDialog(final Activity activity) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try{
                    if (dialog!=null){
                        dialog.dismiss();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    public boolean isNetworkAvailable(Activity activity) {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }


}
