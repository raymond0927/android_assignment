package com.example.recipebook.Activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.recipebook.GlobalVar;
import com.example.recipebook.R;
import com.example.recipebook.SharedPreferenceManager;

import java.util.Locale;

public class SettingActivity extends AppCompatActivity {

    private SharedPreferenceManager mSharedPreferenceManager;
    private final String LOG_TAG = this.getClass().getName();

    private String domain;
    SettingActivity mActivity = this;
    TextView tv_domain;
    Spinner spinner_lan;
    private String language;
    ArrayAdapter<CharSequence> adapter;
    public GlobalVar globalVar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        SharedPreferenceManager.init(this);
        mSharedPreferenceManager = SharedPreferenceManager.getInstance();
        globalVar= (GlobalVar) getApplicationContext();

        try {
            retrieveSharedPreference();

            tv_domain = (TextView) findViewById(R.id.tv_domain);
            spinner_lan = findViewById(R.id.spinner_lan);
            adapter = ArrayAdapter.createFromResource(this,R.array.language,android.R.layout.simple_spinner_item);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner_lan.setAdapter(adapter);

            tv_domain.setText(domain);

            setListeners();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (language.equals("zh")){
            spinner_lan.setSelection(1);
        }else {
            spinner_lan.setSelection(0);
        }


    }

    @Override
    public void onBackPressed() {
        setSharedPreference();
        finish();
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        try {
            retrieveSharedPreference();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void retrieveSharedPreference() {

        if (mSharedPreferenceManager.getDomain() != null) {
            domain = mSharedPreferenceManager.getDomain();
        } else {
            domain = "127.0.0.1:8080";
        }

        if (mSharedPreferenceManager.getLanguage() != null) {
            language = mSharedPreferenceManager.getLanguage();
        }else{
            language = "en";
        }
    }

    public void setSharedPreference() {
        mSharedPreferenceManager.setDomain(domain);
        mSharedPreferenceManager.setLanguage(language);
        mSharedPreferenceManager.saveSharedPreference();

        Log.d(LOG_TAG, "getDomain" + mSharedPreferenceManager.getDomain());
    }


    private void setListeners() {
        tv_domain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alert = new AlertDialog.Builder(mActivity);

                alert.setTitle("Setting");
                alert.setMessage("Set Domain");

                // Set an EditText view to get user input
                final EditText input = new EditText(mActivity);
                input.setText(domain);
                input.setInputType(InputType.TYPE_NUMBER_FLAG_SIGNED);
                alert.setView(input);

                alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        Log.v(LOG_TAG, input.getText().toString());
                        // Do something with value!
                        if (input.getText().length() > 0) {
                            domain = input.getText().toString();
                            setSharedPreference();
                            tv_domain.setText(domain);
                        }
                    }
                });

                alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        // Canceled.
                    }
                });

                alert.show();
            }
        });

        spinner_lan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.v("selected",parent.getItemAtPosition(position).toString()+"");
                language = parent.getItemAtPosition(position).toString().trim();
                if (!parent.getItemAtPosition(position).toString().trim().equals(mSharedPreferenceManager.getLanguage())){
                    setSharedPreference();
                    setLocale(parent.getItemAtPosition(position).toString().trim());
                }
                setSharedPreference();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void setLocale(String lang) {
        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
        Intent refresh = new Intent(mActivity, LoginActivity.class);
        startActivity(refresh);
        finish();
    }

}
