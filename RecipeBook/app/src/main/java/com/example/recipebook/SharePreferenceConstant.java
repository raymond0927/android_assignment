package com.example.recipebook;

/**
 * Created by user on 26/4/2018.
 */

public class SharePreferenceConstant {
    public static final String SHARE_KEY_DOMAIN = "pref_domain";
    public static final String SHARE_KEY_REMEMBER = "pref_isRemember";
    public static final String SHARE_KEY_USERNAME = "pref_username";
    public static final String SHARE_KEY_PASSWORD = "pref_password";
    public static final String SHARE_KEY_LAN = "pref_language";

    private SharePreferenceConstant(){}
}
