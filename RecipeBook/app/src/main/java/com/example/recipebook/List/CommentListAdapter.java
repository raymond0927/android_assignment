package com.example.recipebook.List;

import android.content.Context;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Chronometer;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.example.recipebook.Activity.InfoActivity;
import com.example.recipebook.R;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class CommentListAdapter extends BaseAdapter{
    private Context mContext;
    List<CommentList> commentLists = new ArrayList<>();
    InfoActivity mActivity;
    public MediaPlayer mPlayer;
    public Timer timer = new Timer();
    private boolean isPlaying = false;
    private int lastProgress = 0;
    int pressedPosition = -1;

    public CommentListAdapter(InfoActivity mActivity, List<CommentList> commentLists) {
        this.mActivity = mActivity;
        this.mContext = mActivity.getApplicationContext();
        this.commentLists = commentLists;
    }


    @Override
    public int getCount() {
        return commentLists.size();
    }

    @Override
    public Object getItem(int position) {
        return commentLists.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = View.inflate(mContext, R.layout.comment_list , null);

        LinearLayout ll_comment_list = v.findViewById(R.id.ll_comment_list);
        LinearLayout ll_voice_comment_list = v.findViewById(R.id.ll_voice_comment_list);
        TextView tv_comment_content = v.findViewById(R.id.tv_comment_content);
        TextView tv_comment_author = v.findViewById(R.id.tv_comment_author);
        final SeekBar seekBar_comment_list = v.findViewById(R.id.seekBar_comment_list);
        final ImageView imageViewPlay_comment_list = v.findViewById(R.id.imageViewPlay_comment_list);
        final Chronometer chronometerTimer_comment_list = v.findViewById(R.id.chronometerTimer_comment_list);

        tv_comment_author.setText(commentLists.get(position).getAuthor());
        if (commentLists.get(position).getIsVoice() == 1){
            ll_voice_comment_list.setVisibility(View.VISIBLE);
        }else{
            ll_comment_list.setVisibility(View.VISIBLE);

            tv_comment_content.setText(commentLists.get(position).getComment_eng());

            if (mActivity.globalVar.language.equals("zh")){
                if ( commentLists.get(position).getComment_chi()!=null &&commentLists.get(position).getComment_chi().length()>0){
                    tv_comment_content.setText(commentLists.get(position).getComment_chi());
                }
            }
        }

        imageViewPlay_comment_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageViewPlay_comment_list.setEnabled(false);
                if (isPlaying){
                    stopPlaying(imageViewPlay_comment_list,chronometerTimer_comment_list);
                    imageViewPlay_comment_list.setEnabled(true);
                }else{
                    if ( pressedPosition != position){
                        pressedPosition = position;
                        lastProgress = 0;
                        chronometerTimer_comment_list.setBase(SystemClock.elapsedRealtime());
                        seekBar_comment_list.setProgress(0);
                    }
                    if (pressedPosition ==-1 || pressedPosition == position){
                        pressedPosition = position;
                        startPlaying(commentLists.get(position).getVoice_path(),imageViewPlay_comment_list,seekBar_comment_list,chronometerTimer_comment_list);
                    }

                }
            }
        });

        return v;
    }

    private void stopPlaying(ImageView imageViewPlay_comment_list,Chronometer chronometerTimer) {
        try{
            timer.cancel();
            mPlayer.release();
        }catch (Exception e){
            e.printStackTrace();
        }
        mPlayer = null;
        //showing the play button
        isPlaying = false;
        imageViewPlay_comment_list.setImageResource(R.mipmap.ic_play);
        imageViewPlay_comment_list.setEnabled(true);
        chronometerTimer.stop();

    }

    private void startPlaying(String voiceURL, final ImageView imageViewPlay_comment_list, SeekBar seekBar_comment_list,final Chronometer chronometerTimer) {
        mPlayer = new MediaPlayer();
        try {
            mPlayer.setDataSource(voiceURL);
            mPlayer.prepare();
            mPlayer.start();
        } catch (IOException e) {
            Log.e("LOG_TAG", "prepare() failed");
        }
        //making the imageview pause button
        isPlaying = true;
        imageViewPlay_comment_list.setImageResource(R.mipmap.ic_stop);

        seekBar_comment_list.setProgress(lastProgress);
        mPlayer.seekTo(lastProgress);
        seekBar_comment_list.setMax(mPlayer.getDuration());
        seekUpdation(seekBar_comment_list);
        chronometerTimer.start();
        imageViewPlay_comment_list.setEnabled(true);

        mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                try{
                    timer.cancel();
                }catch (Exception e){
                    e.printStackTrace();
                }
                chronometerTimer.stop();
                isPlaying = false;
                pressedPosition =-1;
                imageViewPlay_comment_list.setImageResource(R.mipmap.ic_play);
            }
        });



        seekBar_comment_list.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if( mPlayer!=null && fromUser ){
                    mPlayer.seekTo(progress);
                    chronometerTimer.setBase(SystemClock.elapsedRealtime() - mPlayer.getCurrentPosition());
                    lastProgress = progress;
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }


    private void seekUpdation(final SeekBar seekBar) {
        timer = new Timer();
        timer.schedule(new TimerTask()
        {
            @Override
            public void run()
            {
                Log.v("timer" ,"run");
                if(mPlayer != null){
                    int mCurrentPosition = mPlayer.getCurrentPosition() ;
                    seekBar.setProgress(mCurrentPosition);
                    lastProgress = mCurrentPosition;
                }
            }
        },  100,100);
    }

}
